#pragma once
#include <Cross/Core.h>
#include <Cross/Window.h>
#include <Cross/Math/Vector2.h>

namespace Cross
{
	class Input
	{
	public:
		struct Key
		{
			static Key a; static Key b; static Key c; static Key d;
			static Key e; static Key f; static Key g; static Key h;
			static Key i; static Key j; static Key k; static Key l;
			static Key m; static Key n; static Key o; static Key p;
			static Key q; static Key r; static Key s; static Key t;
			static Key u; static Key v; static Key w; static Key x;
			static Key y; static Key z;
			static Key space;
			static Key leftShift;
			static Key up, down, left, right;

			Key(Int key);

			Int key;
		};
		static Byte leftButton;
		static Byte rightButton;
		static Byte middleButton;
	private:
		static void KeyDown(const Window::KeyEvent& event, void* data);
		static void KeyUp(const Window::KeyEvent& event, void* data);
		static void MouseMove(const Window::MouseMoveEvent& event, void* data);
		static void MouseDown(const Window::MouseButtonEvent& event, void* data);
		static void MouseUp(const Window::MouseButtonEvent& event, void* data);

		Window* window;
		
		bool keyDown[256];
		bool previousKeyDown[256];

		bool mouseDown[3];
		bool previousMouseDown[3];
		
		Vector2 mousePosition, prevMousePosition, mouseMove;
		
		void calculateMousePosition();
	public:
		Input(Window* window);
		
		void update();
		
		bool isKeyDown(Key key);
		bool isKeyPressed(Key key);
		bool isKeyReleased(Key key);
		
		Vector2 getMousePosition(); // @TODO make this const?
		Vector2 getMouseMove() const;

		bool isMouseDown(Byte button);
		bool isMousePressed(Byte button);
		bool isMouseReleased(Byte button);
	};
}
