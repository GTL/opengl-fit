#include <Cross/Core.h>
#include <Cross/Utility/Hook.h>
#pragma once
#include <string>
#include <map>
#if defined(_WIN32)
#elif defined(unix)
#include <X11/Xlib.h>
#include <vector>
#elif defined(__APPLE__) & defined(__OBJC__)
#import <Cocoa/Cocoa.h>
#import <CrossWindow.h>
#endif

namespace Cross
{
	class Window
	{
		friend class Application;
		friend class Graphics;
		friend class Input;
	public:
		struct KeyEvent
		{
			Int key;
		};
		struct MouseMoveEvent
		{
			Int x, y;
		};
		struct MouseButtonEvent
		{
			Byte button;
		};
	private:

		#if defined(_WIN32)
		static std::map<HWND, Window*> handleMap;

		HWND handle;
		DWORD style;

		static LRESULT CALLBACK windowProcedure(HWND window, UINT message, WPARAM wParam, LPARAM lParam);
		#elif defined(unix)
		::Window handle;
		Display* display;
		XVisualInfo* visual;
		GLXFBConfig* config;
		//XSetWindowAttributes windowAttributes;
		std::vector<XEvent> events;
		#elif defined(__APPLE__)
		#if defined(__OBJC__)
		CrossWindow* window;
		#else
		void* window;
		#endif
		#endif
		
		Hook<KeyEvent> keyDown;
		Hook<KeyEvent> keyUp;
		Hook<MouseMoveEvent> mouseMove;
		Hook<MouseButtonEvent> mouseDown;
		Hook<MouseButtonEvent> mouseUp;
	public:
		#if defined(_WIN32)
		Window(HWND handle, DWORD style);
		#elif defined(unix)
		Window(::Window handle, Display* display, XVisualInfo* visual, GLXFBConfig* config);
		#elif defined(__APPLE__)
		Window(void* handle);
		#endif
		~Window();

		void update();

		#if defined(_WIN32)
		HWND getHandle();
		#elif defined(unix)
		::Window getHandle();
		#elif defined(__APPLE__)
		void* getHandle();
		#endif

		void setTitle(const std::wstring& title);
		std::wstring getTitle();

		void setWidth(UInt width);
		void setHeight(UInt height);
		void setSize(UInt width, UInt height);
		UInt getWidth() const;
		UInt getHeight() const;
		void getSize(UInt* width, UInt* height);
		
		void hookKeyDown(void(*func)(const KeyEvent&, void*), void* data);
		void callKeyDown(const KeyEvent& event);
		void hookKeyUp(void(*func)(const KeyEvent&, void*), void* data);
		void callKeyUp(const KeyEvent& event);
		void hookMouseMove(void(*func)(const MouseMoveEvent&, void*), void* data);
		void callMouseMove(const MouseMoveEvent& event);
		void hookMouseDown(void(*func)(const MouseButtonEvent&, void*), void* data);
		void callMouseDown(const MouseButtonEvent& event);
		void hookMouseUp(void(*func)(const MouseButtonEvent&, void*), void* data);
		void callMouseUp(const MouseButtonEvent& event);
	};
}
