#pragma once
#include <Cross/Core.h>
#include <string>

namespace Cross
{
	class Texture
	{
	protected:
		UInt texture;
		
		Texture();
	public:
		static Texture* loadBitmap(const std::string& fileName);

		Texture(UInt texture);
		~Texture();
		
		UInt getTexture() const;
	};
}
