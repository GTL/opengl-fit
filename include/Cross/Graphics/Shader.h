#pragma once
#include <Cross/Core.h>
#include <Cross/Math/Matrix.h>
#include <string>
#include <map>

namespace Cross
{
	class Texture;
	
	class Shader
	{
		int shaderID;
		int vertexID;
		int fragmentID;
		int textureID;
		std::map<std::string, int> textures;
	public:
		Shader(std::string vertex, std::string fragment);
		~Shader();

		void begin();
		void end();

		void setMatrix(const std::string& name, const Matrix& matrix);
		void setTexture(const std::string& name, Texture* texture);
		void setTexture(const std::string& name, Texture texture);
		void setArray(const std::string& name, Vector4* vectors, UInt size);

		void bindAttribute(UInt index, const std::string& name);

		int getID();
	};
}
