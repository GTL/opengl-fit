#pragma once
#include <Cross/Core.h>
#include <Cross/Graphics/Texture.h>
#include <string>

namespace Cross
{
	class Font
	{
		FT_Face face;

		UInt texture;

		UInt size;

		Font(FT_Face face, UInt size);
	public:
		static Font* loadFont(const std::string& font, UInt size);

		void draw(const std::string& text);

		Texture getTexture();
	};
}