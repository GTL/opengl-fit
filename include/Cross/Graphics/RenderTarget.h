#pragma once
#include <Cross/Core.h>
#include <Cross/Graphics/Texture.h>

namespace Cross
{
	class RenderTarget
	{
		union Viewport { struct { Int x, y; Int width, height; }; Int vp[4]; };
		static Viewport viewport;
		
		UInt textureCount;
		UInt* textures;

		UInt frameBuffer;
		
		UInt renderBuffer;
		
		UInt width, height;
	public:
		RenderTarget(UInt width, UInt height, UInt textures = 1);
		~RenderTarget();
		
		void begin();
		void end();

		Texture getTexture(UInt texture = 0);
	};
}
