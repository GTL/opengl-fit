#pragma once
#include <Cross/Core.h>
#include <Cross/Graphics/SubMesh.h>
#include <map>

namespace Cross
{
	class DynamicSubMesh : public SubMesh
	{
		std::map<UInt, UInt> vertexBuffers;
	public:
		DynamicSubMesh(UInt vertexCount, Type type = TRIANGLES);

		virtual void addBuffer(Vector2* data, UInt index);
		virtual void addBuffer(Vector3* data, UInt index);
		virtual void addBuffer(Vector4* data, UInt index);
		virtual void addBuffer(Color* data, UInt index);

		virtual void updateBuffer(Vector2* data, UInt index);
		virtual void updateBuffer(Vector3* data, UInt index);
		virtual void updateBuffer(Vector4* data, UInt index);
		virtual void updateBuffer(Color* data, UInt index);
	};
}
