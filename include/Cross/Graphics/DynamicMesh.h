#pragma once
#include <Cross/Core.h>
#include <Cross/Graphics/Mesh.h>

namespace Cross
{
	class DynamicMesh : public Mesh
	{
	public:
		SubMesh* addSubMesh(UInt vertexCount, SubMesh::Type type);
	};
};