#pragma once
#include <Cross/Core.h>
#include <Cross/Graphics/SubMesh.h>
#include <vector>
#include <string>

namespace Cross
{
	class Mesh
	{
		std::vector<SubMesh*> subMeshes;
	public:
		virtual SubMesh* addSubMesh(UInt vertexCount, SubMesh::Type type);
		SubMesh* addStaticSubMesh(UInt vertexCount, SubMesh::Type type);
		SubMesh* addDynamicSubMesh(UInt vertexCount, SubMesh::Type type);

		SubMesh* getSubMesh(UInt index) const;

		UInt getSubMeshCount();

		void draw();

		static Mesh* loadObj(const std::string& fileName);
	};
}
