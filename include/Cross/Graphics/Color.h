#pragma once
#include <Cross/Core.h>

namespace Cross
{
	struct Color
	{
		static Color airForceBlue;
		static Color alabamaCrimson;
		static Color aliceBlue;
		static Color alizarinCrimson;
		static Color alloyOrange;
		static Color almond;
		static Color amaranth;
		static Color amber;
		static Color americanRose;
		static Color amethyst;
		static Color androidGreen;
		static Color antiFlashWhite;
		static Color antiqueBrass;
		static Color antiqueFuchsia;
		static Color antiqueRuby;
		static Color antiqueWhite;
		static Color appleGreen;
		static Color apricot;
		static Color aqua;
		static Color aquamarine;
		static Color armyGreen;
		static Color arsenic;
		static Color arylideYellow;
		static Color ashGrey;
		static Color asparagus;
		static Color atomicTangerine;
		static Color auburn;
		static Color aureolin;
		static Color aurometalsaurus;
		static Color azure;
		static Color azureMistWeb;
		static Color babyBlue;
		static Color babyBlueEyes;
		static Color babyPink;
		static Color ballBlue;
		static Color bananaMania;
		static Color bananaYellow;
		static Color barnRed;
		static Color battleshipGrey;
		static Color bazaar;
		static Color beauBlue;
		static Color beaver;
		static Color beige;
		static Color bigDipORuby;
		static Color bisque;
		static Color bistre;
		static Color bittersweet;
		static Color bittersweetShimmer;
		static Color black;
		static Color blanchedAlmond;
		static Color blastOffBronze;
		static Color bleuDeFrance;
		static Color blizzardBlue;
		static Color blond;
		static Color blue;
		static Color blueBell;
		static Color blueGray;
		static Color blueViolet;
		static Color blush;
		static Color bole;
		static Color bondiBlue;
		static Color bone;
		static Color bostonUniversityRed;
		static Color bottleGreen;
		static Color boysenberry;
		static Color brandeisBlue;
		static Color brass;
		static Color brickRed;
		static Color brightCerulean;
		static Color brightGreen;
		static Color brightLavender;
		static Color brightMaroon;
		static Color brightPink;
		static Color brightTurquoise;
		static Color brightUbe;
		static Color brilliantLavender;
		static Color brilliantRose;
		static Color brinkPink;
		static Color britishRacingGreen;
		static Color bronze;
		static Color bubbleGum;
		static Color bubbles;
		static Color buff;
		static Color bulgarianRose;
		static Color burgundy;
		static Color burlywood;
		static Color burntOrange;
		static Color burntUmber;
		static Color byzantine;
		static Color byzantium;
		static Color cadet;
		static Color cadetBlue;
		static Color cadetGrey;
		static Color cadmiumGreen;
		static Color cadmiumOrange;
		static Color cadmiumRed;
		static Color cadmiumYellow;
		static Color cafAuLait;
		static Color cafNoir;
		static Color calPolyGreen;
		static Color cambridgeBlue;
		static Color camel;
		static Color cameoPink;
		static Color camouflageGreen;
		static Color canaryYellow;
		static Color candyAppleRed;
		static Color candyPink;
		static Color capri;
		static Color caputMortuum;
		static Color cardinal;
		static Color caribbeanGreen;
		static Color carmine;
		static Color carminePink;
		static Color carmineRed;
		static Color carnationPink;
		static Color carnelian;
		static Color carolinaBlue;
		static Color carrotOrange;
		static Color ceil;
		static Color celadon;
		static Color celadonBlue;
		static Color celadonGreen;
		static Color celestialBlue;
		static Color cerise;
		static Color cerisePink;
		static Color ceriseRed;
		static Color cerulean;
		static Color ceruleanBlue;
		static Color ceruleanFrost;
		static Color cgBlue;
		static Color cgRed;
		static Color chamoisee;
		static Color champagne;
		static Color charcoal;
		static Color charmPink;
		static Color cherry;
		static Color cherryBlossomPink;
		static Color chestnut;
		static Color chinaPink;
		static Color chinaRose;
		static Color chineseRed;
		static Color chromeYellow;
		static Color cinereous;
		static Color cinnabar;
		static Color cinnamon;
		static Color citrine;
		static Color classicRose;
		static Color cobalt;
		static Color cocoaBrown;
		static Color coffee;
		static Color columbiaBlue;
		static Color congoPink;
		static Color coolBlack;
		static Color coolGrey;
		static Color copper;
		static Color copperRose;
		static Color coquelicot;
		static Color coral;
		static Color coralPink;
		static Color coralRed;
		static Color cordovan;
		static Color corn;
		static Color cornellRed;
		static Color cornflowerBlue;
		static Color cornsilk;
		static Color cosmicLatte;
		static Color cottonCandy;
		static Color cream;
		static Color crimson;
		static Color crimsonGlory;
		static Color cyan;
		static Color daffodil;
		static Color dandelion;
		static Color darkBlue;
		static Color darkBrown;
		static Color darkByzantium;
		static Color darkCandyAppleRed;
		static Color darkCerulean;
		static Color darkChestnut;
		static Color darkCoral;
		static Color darkCyan;
		static Color darkElectricBlue;
		static Color darkGoldenrod;
		static Color darkGray;
		static Color darkGreen;
		static Color darkImperialBlue;
		static Color darkJungleGreen;
		static Color darkKhaki;
		static Color darkLava;
		static Color darkLavender;
		static Color darkMagenta;
		static Color darkMidnightBlue;
		static Color darkOliveGreen;
		static Color darkOrange;
		static Color darkOrchid;
		static Color darkPastelBlue;
		static Color darkPastelGreen;
		static Color darkPastelPurple;
		static Color darkPastelRed;
		static Color darkPink;
		static Color darkPowderBlue;
		static Color darkRaspberry;
		static Color darkRed;
		static Color darkSalmon;
		static Color darkScarlet;
		static Color darkSeaGreen;
		static Color darkSienna;
		static Color darkSlateBlue;
		static Color darkSlateGray;
		static Color darkSpringGreen;
		static Color darkTan;
		static Color darkTangerine;
		static Color darkTaupe;
		static Color darkTerraCotta;
		static Color darkTurquoise;
		static Color darkViolet;
		static Color darkYellow;
		static Color dartmouthGreen;
		static Color davysGrey;
		static Color debianRed;
		static Color deepCarmine;
		static Color deepCarminePink;
		static Color deepCarrotOrange;
		static Color deepCerise;
		static Color deepChampagne;
		static Color deepChestnut;
		static Color deepCoffee;
		static Color deepFuchsia;
		static Color deepJungleGreen;
		static Color deepLilac;
		static Color deepMagenta;
		static Color deepPeach;
		static Color deepPink;
		static Color deepRuby;
		static Color deepSaffron;
		static Color deepSkyBlue;
		static Color denim;
		static Color desert;
		static Color desertSand;
		static Color dimGray;
		static Color dodgerBlue;
		static Color dogwoodRose;
		static Color dollarBill;
		static Color drab;
		static Color dukeBlue;
		static Color earthYellow;
		static Color ecru;
		static Color eggplant;
		static Color eggshell;
		static Color egyptianBlue;
		static Color electricBlue;
		static Color electricCrimson;
		static Color electricCyan;
		static Color electricGreen;
		static Color electricIndigo;
		static Color electricLavender;
		static Color electricLime;
		static Color electricPurple;
		static Color electricUltramarine;
		static Color electricViolet;
		static Color electricYellow;
		static Color emerald;
		static Color etonBlue;
		static Color fallow;
		static Color faluRed;
		static Color famous;
		static Color fandango;
		static Color fashionFuchsia;
		static Color fawn;
		static Color feldgrau;
		static Color fernGreen;
		static Color ferrariRed;
		static Color fieldDrab;
		static Color firebrick;
		static Color fireEngineRed;
		static Color flame;
		static Color flamingoPink;
		static Color flavescent;
		static Color flax;
		static Color floralWhite;
		static Color fluorescentOrange;
		static Color fluorescentPink;
		static Color fluorescentYellow;
		static Color folly;
		static Color frenchBeige;
		static Color frenchBlue;
		static Color frenchLilac;
		static Color frenchLime;
		static Color frenchRaspberry;
		static Color frenchRose;
		static Color fuchsia;
		static Color fuchsiaPink;
		static Color fuchsiaRose;
		static Color fulvous;
		static Color fuzzyWuzzy;
		static Color gainsboro;
		static Color gamboge;
		static Color ghostWhite;
		static Color ginger;
		static Color glaucous;
		static Color glitter;
		static Color goldenBrown;
		static Color goldenPoppy;
		static Color goldenYellow;
		static Color goldenrod;
		static Color grannySmithApple;
		static Color gray;
		static Color grayAsparagus;
		static Color greenYellow;
		static Color grullo;
		static Color guppieGreen;
		static Color halayBe;
		static Color hanBlue;
		static Color hanPurple;
		static Color hansaYellow;
		static Color harlequin;
		static Color harvardCrimson;
		static Color harvestGold;
		static Color heartGold;
		static Color heliotrope;
		static Color hollywoodCerise;
		static Color honeydew;
		static Color honoluluBlue;
		static Color hookersGreen;
		static Color hotMagenta;
		static Color hotPink;
		static Color hunterGreen;
		static Color iceberg;
		static Color icterine;
		static Color imperialBlue;
		static Color inchworm;
		static Color indiaGreen;
		static Color indianRed;
		static Color indianYellow;
		static Color indigo;
		static Color internationalKleinBlue;
		static Color iris;
		static Color isabelline;
		static Color islamicGreen;
		static Color ivory;
		static Color jade;
		static Color jasmine;
		static Color jasper;
		static Color jazzberryJam;
		static Color jonquil;
		static Color juneBud;
		static Color jungleGreen;
		static Color kellyGreen;
		static Color kuCrimson;
		static Color laSalleGreen;
		static Color languidLavender;
		static Color lapisLazuli;
		static Color laserLemon;
		static Color laurelGreen;
		static Color lava;
		static Color lavenderBlue;
		static Color lavenderBlush;
		static Color lavenderGray;
		static Color lavenderIndigo;
		static Color lavenderMagenta;
		static Color lavenderMist;
		static Color lavenderPink;
		static Color lavenderPurple;
		static Color lavenderRose;
		static Color lawnGreen;
		static Color lemon;
		static Color lemonChiffon;
		static Color lemonLime;
		static Color lightApricot;
		static Color lightBlue;
		static Color lightBrown;
		static Color lightCarminePink;
		static Color lightCoral;
		static Color lightCornflowerBlue;
		static Color lightCrimson;
		static Color lightCyan;
		static Color lightFuchsiaPink;
		static Color lightGoldenrodYellow;
		static Color lightGray;
		static Color lightGreen;
		static Color lightKhaki;
		static Color lightPastelPurple;
		static Color lightPink;
		static Color lightRedOchre;
		static Color lightSalmon;
		static Color lightSalmonPink;
		static Color lightSeaGreen;
		static Color lightSkyBlue;
		static Color lightSlateGray;
		static Color lightTaupe;
		static Color lightThulianPink;
		static Color lightYellow;
		static Color lilac;
		static Color limeGreen;
		static Color lincolnGreen;
		static Color linen;
		static Color lion;
		static Color liver;
		static Color lust;
		static Color magenta;
		static Color magicMint;
		static Color magnolia;
		static Color mahogany;
		static Color maize;
		static Color majorelleBlue;
		static Color malachite;
		static Color manatee;
		static Color mangoTango;
		static Color mantis;
		static Color mardiGras;
		static Color mauve;
		static Color mauveTaupe;
		static Color mauvelous;
		static Color mayaBlue;
		static Color meatBrown;
		static Color mediumAquamarine;
		static Color mediumBlue;
		static Color mediumCandyAppleRed;
		static Color mediumCarmine;
		static Color mediumChampagne;
		static Color mediumElectricBlue;
		static Color mediumJungleGreen;
		static Color mediumLavenderMagenta;
		static Color mediumOrchid;
		static Color mediumPersianBlue;
		static Color mediumPurple;
		static Color mediumRedViolet;
		static Color mediumSeaGreen;
		static Color mediumSlateBlue;
		static Color mediumSpringBud;
		static Color mediumSpringGreen;
		static Color mediumTaupe;
		static Color mediumTealBlue;
		static Color mediumTurquoise;
		static Color mediumVermilion;
		static Color mediumVioletRed;
		static Color mellowYellow;
		static Color melon;
		static Color midnightBlue;
		static Color mikadoYellow;
		static Color mint;
		static Color mintCream;
		static Color mintGreen;
		static Color mistyRose;
		static Color moccasin;
		static Color modeBeige;
		static Color moonstoneBlue;
		static Color mordantRed;
		static Color mossGreen;
		static Color mountainMeadow;
		static Color mountbattenPink;
		static Color mulberry;
		static Color munsell;
		static Color mustard;
		static Color myrtle;
		static Color msuGreen;
		static Color nadeshikoPink;
		static Color napierGreen;
		static Color naplesYellow;
		static Color navajoWhite;
		static Color navyBlue;
		static Color neonCarrot;
		static Color neonFuchsia;
		static Color neonGreen;
		static Color newYorkPink;
		static Color nonPhotoBlue;
		static Color oceanBoatBlue;
		static Color ochre;
		static Color officeGreen;
		static Color oldGold;
		static Color oldLace;
		static Color oldLavender;
		static Color oldMauve;
		static Color oldRose;
		static Color olive;
		static Color oliveDrab;
		static Color olivine;
		static Color onyx;
		static Color operaMauve;
		static Color orangePeel;
		static Color orangeRed;
		static Color orchid;
		static Color otterBrown;
		static Color outerSpace;
		static Color outrageousOrange;
		static Color oxfordBlue;
		static Color ouCrimsonRed;
		static Color pakistanGreen;
		static Color palatinateBlue;
		static Color palatinatePurple;
		static Color paleAqua;
		static Color paleBlue;
		static Color paleBrown;
		static Color paleCarmine;
		static Color paleCerulean;
		static Color paleChestnut;
		static Color paleCopper;
		static Color paleCornflowerBlue;
		static Color paleGold;
		static Color paleGoldenrod;
		static Color paleGreen;
		static Color paleLavender;
		static Color paleMagenta;
		static Color palePink;
		static Color palePlum;
		static Color paleRedViolet;
		static Color paleRobinEggBlue;
		static Color paleSilver;
		static Color paleSpringBud;
		static Color paleTaupe;
		static Color paleVioletRed;
		static Color pansyPurple;
		static Color papayaWhip;
		static Color parisGreen;
		static Color pastelBlue;
		static Color pastelBrown;
		static Color pastelGray;
		static Color pastelGreen;
		static Color pastelMagenta;
		static Color pastelOrange;
		static Color pastelPink;
		static Color pastelPurple;
		static Color pastelRed;
		static Color pastelViolet;
		static Color pastelYellow;
		static Color patriarch;
		static Color paynesGrey;
		static Color peach;
		static Color peachOrange;
		static Color peachPuff;
		static Color peachYellow;
		static Color pear;
		static Color pearl;
		static Color pearlAqua;
		static Color pearlyPurple;
		static Color peridot;
		static Color periwinkle;
		static Color persianGreen;
		static Color persianIndigo;
		static Color persianOrange;
		static Color persianPink;
		static Color persianPlum;
		static Color persianRed;
		static Color persianRose;
		static Color persimmon;
		static Color peru;
		static Color phlox;
		static Color phthaloBlue;
		static Color phthaloGreen;
		static Color piggyPink;
		static Color pineGreen;
		static Color pink;
		static Color pinkLace;
		static Color pinkPearl;
		static Color pinkSherbet;
		static Color pistachio;
		static Color platinum;
		static Color portlandOrange;
		static Color princetonOrange;
		static Color prune;
		static Color prussianBlue;
		static Color psychedelicPurple;
		static Color puce;
		static Color pumpkin;
		static Color purpleHeart;
		static Color purpleMountainMajesty;
		static Color purplePizzazz;
		static Color purpleTaupe;
		static Color quartz;
		static Color rackley;
		static Color radicalRed;
		static Color raspberry;
		static Color raspberryGlace;
		static Color raspberryPink;
		static Color raspberryRose;
		static Color rawUmber;
		static Color razzleDazzleRose;
		static Color razzmatazz;
		static Color red;
		static Color redBrown;
		static Color redDevil;
		static Color redOrange;
		static Color redViolet;
		static Color redwood;
		static Color regalia;
		static Color richBlack;
		static Color richBrilliantLavender;
		static Color richCarmine;
		static Color richElectricBlue;
		static Color richLavender;
		static Color richLilac;
		static Color richMaroon;
		static Color rifleGreen;
		static Color robinEggBlue;
		static Color rose;
		static Color roseBonbon;
		static Color roseEbony;
		static Color roseGold;
		static Color roseMadder;
		static Color rosePink;
		static Color roseQuartz;
		static Color roseTaupe;
		static Color roseVale;
		static Color rosewood;
		static Color rossoCorsa;
		static Color rosyBrown;
		static Color royalAzure;
		static Color royalFuchsia;
		static Color royalPurple;
		static Color royalYellow;
		static Color rubineRed;
		static Color ruby;
		static Color ruddy;
		static Color ruddyBrown;
		static Color ruddyPink;
		static Color rufous;
		static Color russet;
		static Color rust;
		static Color sacramentoStateGreen;
		static Color saddleBrown;
		static Color saffron;
		static Color stPatricksBlue;
		static Color salmon;
		static Color salmonPink;
		static Color sand;
		static Color sandDune;
		static Color sandstorm;
		static Color sandyBrown;
		static Color sandyTaupe;
		static Color sangria;
		static Color sapGreen;
		static Color sapphire;
		static Color satinSheenGold;
		static Color scarlet;
		static Color schoolBusYellow;
		static Color screaminGreen;
		static Color seaBlue;
		static Color seaGreen;
		static Color sealBrown;
		static Color seashell;
		static Color selectiveYellow;
		static Color sepia;
		static Color shadow;
		static Color shamrockGreen;
		static Color shockingPink;
		static Color sienna;
		static Color silver;
		static Color sinopia;
		static Color skobeloff;
		static Color skyBlue;
		static Color skyMagenta;
		static Color slateBlue;
		static Color slateGray;
		static Color smokeyTopaz;
		static Color smokyBlack;
		static Color snow;
		static Color spiroDiscoBall;
		static Color springBud;
		static Color springGreen;
		static Color steelBlue;
		static Color stilDeGrainYellow;
		static Color stizza;
		static Color stormcloud;
		static Color straw;
		static Color sunglow;
		static Color sunset;
		static Color tan;
		static Color tangelo;
		static Color tangerine;
		static Color tangerineYellow;
		static Color tangoPink;
		static Color taupe;
		static Color taupeGray;
		static Color teaGreen;
		static Color teal;
		static Color tealBlue;
		static Color tealGreen;
		static Color telemagenta;
		static Color terraCotta;
		static Color thistle;
		static Color thulianPink;
		static Color tickleMePink;
		static Color tiffanyBlue;
		static Color tigersEye;
		static Color timberwolf;
		static Color titaniumYellow;
		static Color tomato;
		static Color toolbox;
		static Color topaz;
		static Color tractorRed;
		static Color trolleyGrey;
		static Color tropicalRainForest;
		static Color trueBlue;
		static Color tuftsBlue;
		static Color tumbleweed;
		static Color turkishRose;
		static Color turquoise;
		static Color turquoiseBlue;
		static Color turquoiseGreen;
		static Color tuscanRed;
		static Color twilightLavender;
		static Color tyrianPurple;
		static Color uaBlue;
		static Color uaRed;
		static Color ube;
		static Color uclaBlue;
		static Color uclaGold;
		static Color ufoGreen;
		static Color ultramarine;
		static Color ultramarineBlue;
		static Color ultraPink;
		static Color umber;
		static Color unitedNationsBlue;
		static Color universityOfCaliforniaGold;
		static Color unmellowYellow;
		static Color upForestGreen;
		static Color upMaroon;
		static Color upsdellRed;
		static Color urobilin;
		static Color uscCardinal;
		static Color uscGold;
		static Color utahCrimson;
		static Color vanilla;
		static Color vegasGold;
		static Color venetianRed;
		static Color verdigris;
		static Color vermilion;
		static Color veronica;
		static Color violet;
		static Color viridian;
		static Color vividAuburn;
		static Color vividBurgundy;
		static Color vividCerise;
		static Color vividTangerine;
		static Color vividViolet;
		static Color warmBlack;
		static Color waterspout;
		static Color wenge;
		static Color wheat;
		static Color white;
		static Color whiteSmoke;
		static Color wildBlueYonder;
		static Color wildStrawberry;
		static Color wildWatermelon;
		static Color wine;
		static Color wisteria;
		static Color yaleBlue;
		static Color yellow;
		static Color yellowOchre;
		static Color yellowGreen;
		static Color yellowOrange;
		static Color zaffre;
		static Color zinnwalditeBrown;

		static Color makeHSV(UInt hue, Byte saturation, Byte value);

		Byte r, g, b, a;

		Color();
		Color(Byte r, Byte g, Byte b, Byte a = 255);
	};
}