#pragma once
#include <Cross/Core.h>
#include <Cross/Graphics/Color.h>

namespace Cross
{
	class Window;
	class Application;

	class Graphics
	{
		friend class Application;

		#if defined(_WIN32)
		PIXELFORMATDESCRIPTOR pfd;
		HDC deviceContext;
		UInt pixelFormat;
		HGLRC renderContext;
		#elif defined(unix)
		GLXContext context;
		#endif
	public:
		Graphics(Window* window);

		void clear(Cross::Color color = Cross::Color::black);
	};
}
