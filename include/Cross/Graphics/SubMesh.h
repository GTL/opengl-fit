#pragma once
#include <Cross/Core.h>

namespace Cross
{
	struct Vector2;
	struct Vector3;
	struct Vector4;
	struct Color;

	class SubMesh
	{
	public:
		enum Type
		{
			POINTS, LINES, LINE_LOOP, LINE_STRIP, TRIANGLES, TRIANGLE_STRIP, TRIANGLE_FAN, QUADS, QUAD_STRIP, POLYGON
		};
	protected:
		UInt vertexArray;

		UInt vertexCount;

		Type type;
	public:
		SubMesh(UInt vertexCount, Type type = TRIANGLES);

		virtual void addBuffer(Vector2* data, UInt index);
		virtual void addBuffer(Vector3* data, UInt index);
		virtual void addBuffer(Vector4* data, UInt index);
		virtual void addBuffer(Color* data, UInt index);

		virtual void updateBuffer(Vector2* data, UInt index);
		virtual void updateBuffer(Vector3* data, UInt index);
		virtual void updateBuffer(Vector4* data, UInt index);
		virtual void updateBuffer(Color* data, UInt index);

		void setType(Type type);
		Type getType() const;

		void draw();
	};
}
