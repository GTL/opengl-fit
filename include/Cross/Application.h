#pragma once
#include <Cross/Core.h>

namespace Cross
{
	class Window;
	class Graphics;
	class Input;

	class Application
	{
		static Application* application;
		
		static bool running;
		#if defined(_WIN32)
		static wchar_t* className;
		#endif

		float __previousTime;
	#if defined(__OBJC__)
	public:
	#else
	protected:
	#endif
		Window* window;
	#if defined(__OBJC__)
	protected:
	#endif
		Graphics* graphics;
		Input* input;
	public:
		static Application* getApplication();
		
		static void quit();
		static void sleep(UInt milliseconds);
		
		Application();

		virtual void initialize();
		virtual void update(float dt);
		virtual void draw();
		virtual void uninitialize();

		void run();
	};
}