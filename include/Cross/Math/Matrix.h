#pragma once

namespace Cross
{
	struct Vector2;
	struct Vector3;
	struct Vector4;

	struct Matrix
	{
		union
		{
			struct
			{
				float m11; float m12; float m13; float m14;
				float m21; float m22; float m23; float m24;
				float m31; float m32; float m33; float m34;
				float m41; float m42; float m43; float m44;
			};
			float m[4][4];
		};

		static const Matrix zero;
		static const Matrix identity;

		static Matrix inverse(const Matrix& matrix);
		static Matrix transpose(const Matrix& matrix);
		static Matrix makeTranslation(const Vector2& translation);
		static Matrix makeTranslation(const Vector3& translation);
		static Matrix makeScaling(const Vector2& scaling);
		static Matrix makeScaling(const Vector3& scaling);
		static Matrix makeRotation(const float angle);
		static Matrix makeRotationX(const float angle);
		static Matrix makeRotationY(const float angle);
		static Matrix makeRotationZ(const float angle);
		static Matrix makePerspective(const float fovY, const float aspectRatio, const float zNear, const float zFar);
		static Matrix makeLookAt(const Vector3& eye, const Vector3& at, const Vector3& up);
		static Matrix makeOrtho(float left, float top, float right, float bottom);
		static Matrix makeFrustum(const float fovY, const float aspectRatio, const float zNear, const float zFar);

		Matrix();
		Matrix(const float m11, const float m12, const float m21, const float m22);
		Matrix(const float m11, const float m12, const float m13, const float m21, const float m22, const float m23, const float m31, const float m32, const float m33);
		Matrix(const float m11, const float m12, const float m13, const float m14, const float m21, const float m22, const float m23, const float m24,
			   const float m31, const float m32, const float m33, const float m34, const float m41, const float m42, const float m43, const float m44);
		Matrix(const Matrix& copy);

		void operator=(const Matrix& operand);
		float* operator[](const unsigned int index);
		//operator const float*();
		Matrix operator*(const Matrix& operand) const;
		Matrix& operator*=(const Matrix& operand);
		Matrix& operator*=(const float operand);
		Matrix& operator/=(const float operand);

		float getDeterminant() const;

		const float* getData() const;
	};
}