#pragma once

namespace Cross
{
	struct Matrix;

	struct Vector2
	{
		union
		{
			struct
			{
				float x, y;
			};
			float m[2];
		};

		static const Vector2 zero;
		static const Vector2 one;

		static float distance(const Vector2& vectorA, const Vector2& vectorB);
		static float distanceSquared(const Vector2& vectorA, const Vector2& vectorB);
		static float distanceManhattan(const Vector2& vectorA, const Vector2& vectorB);
		static Vector2 normalized(const Vector2& vector);
		static float dotProduct(const Vector2& vectorA, const Vector2& vectorB);
		static float perpProduct(const Vector2& vectorA, const Vector2& vectorB);

		Vector2();
		Vector2(const Vector2& copy);
		Vector2(float x, float y);

		Vector2& operator=(const Vector2& operand);
		float& operator[](unsigned int index);
		const float operator[](unsigned int index) const;
		Vector2 operator*(const float operand) const;
		Vector2& operator*=(const float operand);
		Vector2 operator*(const Vector2& operand) const;
		Vector2& operator*=(const Vector2& operand);
		Vector2 operator*(const Matrix& operand) const;
		Vector2& operator*=(const Matrix& operand);
		Vector2 operator/(const float operand) const;
		Vector2& operator/=(const float operand);
		Vector2 operator+(const Vector2& operand) const;
		Vector2& operator+=(const Vector2& operand);
		Vector2 operator-() const;
		Vector2 operator-(const Vector2& operand) const;
		Vector2& operator-=(const Vector2& operand);
		Vector2 operator%(const Vector2& operand) const;
		Vector2& operator%=(const Vector2& operand);
		bool operator==(const Vector2& operand) const;
		bool operator!=(const Vector2& operand) const;

		void set(const float x, const float y);
		void clear();
		float getLength() const;
		float getLengthSquared() const;
		float getLengthManhattan() const;
		void normalize();
		float getDirection() const;
		void setDirection(float angle);
	};
}