#pragma once

namespace Cross
{
	struct Vector2;

	struct Vector3
	{
		union
		{
			struct
			{
				float x, y, z;
			};
			float m[3];
		};

		static const Vector3 zero;
		static const Vector3 one;

		static float distance(const Vector3& vectorA, const Vector3& vectorB);
		static float distanceSquared(const Vector3& vectorA, const Vector3& vectorB);
		static float distanceManhattan(const Vector3& vectorA, const Vector3& vectorB);
		static Vector3 normalized(const Vector3& vector);
		static float dotProduct(const Vector3& vectorA, const Vector3& vectorB);
		static Vector3 crossProduct(const Vector3& vectorA, const Vector3& vectorB);
		static void orthonormalBasis(Vector3* vectorA, Vector3* vectorB, Vector3* vectorC);

		Vector3();
		Vector3(const Vector3& copy);
		Vector3(float x, float y, float z);
		Vector3(const Vector2& vector, float z);

		operator Vector2() const;

		Vector3& operator=(const Vector3& operand);
		float& operator[](const unsigned int index);
		const float operator[](unsigned int index) const;
		Vector3 operator*(const float operand) const;
		Vector3& operator*=(const float operand);
		Vector3 operator/(const float operand) const;
		Vector3& operator/=(const float operand);
		Vector3 operator+(const Vector3& operand) const;
		Vector3& operator+=(const Vector3& operand);
		Vector3 operator-() const;
		Vector3 operator-(const Vector3& operand) const;
		Vector3& operator-=(const Vector3& operand);
		Vector3 operator%(const Vector3& operand) const;
		Vector3& operator%=(const Vector3& operand);
		bool operator==(const Vector3& operand) const;
		bool operator!=(const Vector3& operand) const;

		void set(const float x, const float y, const float z);
		void clear();
		float getLength() const;
		float getLengthSquared() const;
		float getLengthManhattan() const;
		void normalize();
	};
}