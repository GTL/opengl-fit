#pragma once
#include <Cross/Core.h>
#include <map>

namespace Cross
{
	template <class T> class Hook
	{
		UInt hookID;
		
		std::map<UInt, std::pair<void(*)(const T&, void*), void*> > hooks;
	public:
		Hook();
		
		UInt hook(void(*func)(const T&, void*), void* data);
		void unhook(UInt hookID);
		
		void call(const T& event);
	};
}

template <class T> Cross::Hook<T>::Hook()
{
	hookID = 0;
}

template <class T> UInt Cross::Hook<T>::hook(void(*func)(const T&, void*), void* data)
{
	hooks.insert(std::make_pair(hookID, std::make_pair(func, data)));
	return hookID++;
}

template <class T> void Cross::Hook<T>::unhook(UInt hookID)
{
}

template <class T> void Cross::Hook<T>::call(const T& event)
{
#if defined(__APPLE__) | defined(unix)
	for (typename std::map<const unsigned int, std::pair<void (*)(const T&, void*), void*> >::iterator i = hooks.begin(); i != hooks.end(); i++)
#else
	for (std::map<UInt, std::pair<void(*)(const T&, void*), void*> >::iterator i = hooks.begin(); i != hooks.end(); i++)
#endif
	{
		i->second.first(event, i->second.second);
	}
}
