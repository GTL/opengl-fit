#pragma once
#include <Cross/Core.h>
#include <sstream>

namespace Cross
{
	namespace Utility
	{
		template <class T> T build(Byte* data);
		template <class T> T stringTo(const std::string& string);
		void zeroMemory(void* source, unsigned int size);
	};
}

template <class T> T Cross::Utility::build(Byte* data)
{
	T ret;
	for (UInt i = 0; i < sizeof(T); i++)
	{
		((Byte*)&ret)[i] = data[i];
	}
	return ret;
}

template <class T> T Cross::Utility::stringTo(const std::string& string)
{
	std::stringstream ss(std::stringstream::in | std::stringstream::out);
	ss << string;
	ss.seekp(ss.beg);
	T ret;
	ss >> ret;
	return ret;
}