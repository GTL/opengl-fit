#pragma once
#include <Cross/Core.h>
#include <string>

// @TODO make this utility a little more useful

namespace Cross
{
	class File
	{
	public:
		static bool getText(const std::string& fileName, std::string& result);
		static bool getData(const std::string& fileName, Byte** data, UInt* size = nullptr);
	};
}