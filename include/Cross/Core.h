#pragma once
#if defined(_WIN32)
#include <Windows.h>
//#include <WinSock2.h>
#include <GL/glew.h>
#include <GL/gl.h>
#elif defined(unix)
#define Font X11Font
#include <X11/Xlib.h>
#include <GL/glew.h>
#include <GL/glxew.h>
#include <GL/gl.h>
#undef Font
//#include <GL/glx.h>
#elif defined(__APPLE__)
#include <GL/glew.h>
#endif
#include <ft2build.h>
#include FT_FREETYPE_H

extern FT_Library __freetypeLibrary; // @TODO make sure this is okay as a global variable

#define freeptr(ptr) if (ptr) { delete ptr; ptr = nullptr; }

// boolean
#if !defined(__OBJC__)
typedef bool Boolean;
#endif

// character type
typedef char Char;

// integer types
#if !defined(__OBJC__)
typedef unsigned char Byte;
#endif
typedef GLshort Short;
typedef GLushort UShort;
typedef GLint Int;
typedef GLuint UInt;

// integer type with sizes
typedef char Int8;
typedef unsigned char UInt8;
typedef Short Int16;
typedef UShort UInt16;
typedef Int Int32;
typedef UInt UInt32;

// float point numbers
typedef GLfloat Float;
typedef GLdouble Double;
