#import <Cocoa/Cocoa.h>

@interface CrossEngineAppDelegate : NSObject <NSApplicationDelegate>
{
    NSWindow* window;
}

@property (assign) IBOutlet NSWindow* window;

@end
