#pragma once
#include <Cross/Cross.h>
#include <Demos/Test/Camera.h>

namespace Test
{
	class TestApp : public Cross::Application
	{
		float hue;

		Cross::Shader* screenShader, * simpleShader;

		Cross::Mesh* mesh, * meshBox;
		
		Cross::Texture* texture;
		
		Cross::RenderTarget* renderTarget;

		Cross::Font* fontGunplay;

		Camera* camera;
	public:
		void initialize();
		void update(float dt);
		void draw();
		void uninitialize();
	};
}
