#pragma once
#include <Cross/Cross.h>

namespace Test
{
	class Camera
	{
		Cross::Input* input;

		Cross::Vector3 position;

		float direction, pitch;

		Cross::Matrix view;
	public:
		Camera(Cross::Input* input);

		void update(float dt);

		Cross::Matrix getView() const;

		void setPosition(const Cross::Vector3& position);
		Cross::Vector3 getPosition() const;

		void setDirection(float direction);
		float getDirection() const;

		void setPitch(float pitch);
		float getPitch() const;

		Cross::Vector3 getForward() const;
		Cross::Vector3 getRight() const;
		Cross::Vector3 getUp() const;
	};
}