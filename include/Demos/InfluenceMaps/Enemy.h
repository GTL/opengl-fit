#pragma once
#include <Cross/Cross.h>

namespace InfluenceMaps
{
	class Enemy
	{
		Cross::Vector3 position;
	public:
		Enemy(const Cross::Vector3& position);

		void update(float dt);

		void setPosition(const Cross::Vector3& position);
		Cross::Vector3 getPosition() const;
	};
}
