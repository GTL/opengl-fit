#pragma once
#include <Cross/Cross.h>
#include <Demos/Test/Camera.h>
#include <Demos/InfluenceMaps/Ally.h>
#include <Demos/InfluenceMaps/Enemy.h>
#include <vector>

namespace InfluenceMaps
{
	class InfluenceAppGPU : public Cross::Application
	{
		Cross::Random random;

		Cross::Shader* shaderScreen, * shaderSimple, * shaderInfluence, * shaderComboInfluence;

		Cross::Mesh* meshQuad, * meshBox, * meshGround;
		
		Cross::Vector4 meshInfluenceData[50 * 4];

		Cross::DynamicMesh* meshInfluence;

		Cross::Texture* textureBolt, * textureSmile, * textureFalloff1;
		
		Cross::RenderTarget* influenceDanger, * influenceCover, * influenceCombo;

		Test::Camera* camera;

		std::vector<Ally> allies;
		std::vector<Enemy> enemies;
	public:
		void initialize();
		void update(float dt);
		void draw();
		void uninitialize();

		void generateInfluenceMaps();
	};
}
