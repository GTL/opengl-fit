#pragma once
#include <Cross/Cross.h>
#include <string>

namespace FiT
{
	struct FiTEntity
	{
		std::string type;
		Cross::Texture* texture;
		Cross::Vector3 position, velocity;
		float channels[8];

		void update(float dt);
	};
}
