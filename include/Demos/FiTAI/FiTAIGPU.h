#pragma once
#include <Cross/Cross.h>
#include <Demos/Test/Camera.h>
#include <Demos/FiTAI/FiTEntity.h>
#include <vector>

namespace FiT
{
	class FiTAIGPU : public Cross::Application
	{
		Cross::Random random;

		Cross::Shader* shaderScreen, * shaderSimple, * shaderFit1, * shaderFit2, * shaderFitClean;

		Cross::Mesh* meshQuad, * meshBox, * meshGround;

		Cross::Vector4 meshInfluenceData[100 * 4];
		Cross::Vector4 meshInfluence1Channels1[100 * 4];
		Cross::Vector4 meshInfluence1Channels2[100 * 4];
		Cross::Vector4 meshInfluence2Channels1[100];
		Cross::Vector4 meshInfluence2Channels2[100];

		Cross::DynamicMesh* meshInfluence1, * meshInfluence2;

		Cross::Texture* textureBolt, * textureSmile, * textureFalloff1;

		Cross::RenderTarget* influences, * influenceClean;

		Cross::Font* fontGunplay;

		Test::Camera* camera;

		std::vector<FiTEntity> entities;

		UInt mapToDraw;

		bool useArrayMethod;
		float timeToCalculate;
	public:
		void initialize();
		void update(float dt);
		void draw();
		void uninitialize();

		void generateInfluenceMaps();
		void calculateCalculationTime();
	};
}
