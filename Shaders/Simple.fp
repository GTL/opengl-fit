#version 120

uniform sampler2D texture;

varying vec2 fragTexCoord;

void main(void)
{
	gl_FragColor = texture2D(texture, fragTexCoord);
	gl_FragColor.a = 1;
	//gl_FragColor = vec4(fragTexCoord, 0, 1);
}