#version 120

uniform sampler2D texture1;
uniform sampler2D texture2;

varying vec2 fragTexCoord;

void main(void)
{
	vec4 col1 = texture2D(texture1, fragTexCoord);
	vec4 col2 = texture2D(texture2, fragTexCoord);
	float amt = (col1.g + col2.g) - (col1.r + col2.r);
	gl_FragColor = vec4(0, 0, 0, 1);
	if (amt < 0)
	{
		gl_FragColor.r = -amt;
	}
	else
	{
		gl_FragColor.g = amt;
	}
}