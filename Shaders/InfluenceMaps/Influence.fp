#version 120

uniform sampler2D texture;

varying vec3 fragTexCoord;

void main(void)
{
	gl_FragColor.a = texture2D(texture, fragTexCoord.xy).a;

	float amt = fragTexCoord.z;
	if (amt < 0)
	{
		amt *= -1;
	}
	amt = min(amt, 1);

	if (fragTexCoord.z < 0)
	{
		gl_FragColor.r = gl_FragColor.a * amt;
		gl_FragColor.g = 0;
	}
	else
	{
		gl_FragColor.g = gl_FragColor.a * amt;
		gl_FragColor.r = 0;
	}
	gl_FragColor.b = 0;
}