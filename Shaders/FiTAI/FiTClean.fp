#version 120

uniform sampler2D texture;

varying vec2 fragTexCoord;

void main(void)
{
	vec4 col = texture2D(texture, fragTexCoord);
	float amt = col.g - col.r;
	gl_FragColor = vec4(0, 0, 0, 1);
	if (amt < 0)
	{
		gl_FragColor.r = -amt;
	}
	else
	{
		gl_FragColor.g = amt;
	}
}