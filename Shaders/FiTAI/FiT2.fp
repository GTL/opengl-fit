#version 120
#extension GL_EXT_gpu_shader4 : enable

uniform sampler2D texture;
uniform mat4 channelsMatrix1;
uniform mat4 channelsMatrix2;
uniform mat4 channelsMatrix3;
uniform mat4 channelsMatrix4;

uniform vec4 channels1[100];
uniform vec4 channels2[100];

varying vec3 fragTexCoord;

void main(void)
{
	float channels[8];
	channels[0] = channels1[int(fragTexCoord.z)].x;
	channels[1] = channels1[int(fragTexCoord.z)].y;
	channels[2] = channels1[int(fragTexCoord.z)].z;
	channels[3] = channels1[int(fragTexCoord.z)].w;
	channels[4] = channels2[int(fragTexCoord.z)].x;
	channels[5] = channels2[int(fragTexCoord.z)].y;
	channels[6] = channels2[int(fragTexCoord.z)].z;
	channels[7] = channels2[int(fragTexCoord.z)].w;
	
	vec4 color[8];
	for (int i = 0; i < 8; i++)
	{
		color[i].a = texture2D(texture, fragTexCoord.xy).a;
		
		float amount = 0;
		
		for (int j = 0; j < 8; j++)
		{
			float channelAmount = channels[j];
			int matIndex = 0;
			int matX = i % 4;
			int matY = j % 4;
			if (i < 4)
			{
				if (j < 4) 
				{
					channelAmount *= channelsMatrix1[matX][matY];
				}
				else
				{
					channelAmount *= channelsMatrix2[matX][matY];
				}
			}
			else 
			{
				if (j < 4)
				{
					channelAmount *= channelsMatrix3[matX][matY];
				}
				else
				{
					channelAmount *= channelsMatrix4[matX][matY];
				}
			}
			amount += channelAmount;
		}
		
		bool negative = amount < 0;
	
		if (amount < 0)
		{
			amount *= -1;
		}
		amount = min(amount, 1);
	
		if (negative)
		{
			color[i].r = color[i].a * amount;
			color[i].g = 0;
		}
		else
		{
			color[i].g = color[i].a * amount;
			color[i].r = 0;
		}
		color[i].b = 0;
	}
	
	gl_FragData[0] = color[0];
	gl_FragData[1] = color[1];
	gl_FragData[2] = color[2];
	gl_FragData[3] = color[3];
	gl_FragData[4] = color[4];
	gl_FragData[5] = color[5];
	gl_FragData[6] = color[6];
	gl_FragData[7] = color[7];
}