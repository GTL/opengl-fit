#include <Demos/FiTAI/FiTEntity.h>
using namespace Cross;

void FiT::FiTEntity::update(float dt)
{
	if (type == "Wander")
	{
		// generate a random point ahead
		Vector3 pointAhead = position + (Vector3::normalized(velocity) * 80.0f);
		float circleRadius = 30;
		float angle = float(rand() % 360);
		Vector3 pos = pointAhead + Vector3(Math::cos(Math::degToRad(angle)) * circleRadius, 0, Math::sin(Math::degToRad(angle)) * circleRadius);

		// seek the point
		Vector3 dir = pos - position;
		dir.normalize();
		dir *= 5;
		velocity += dir;
		if (velocity.getLength() > 10)
		{
			velocity.normalize();
			velocity *= 10;
		}

		// move
		position += velocity * dt;

		// wrap
		if (position.x < -50) position.x = 50;
		if (position.z < -50) position.z = 50;
		if (position.x > 50) position.x = -50;
		if (position.z > 50) position.z = -50;
	}
}
