#include <Demos/FiTAI/FiTAIGPU.h>
#include <iostream>
#include <fstream>
#include <string>
using namespace Cross;
using namespace std;

void FiT::FiTAIGPU::initialize()
{
	// initialize the base app
	Application::initialize();

	// set the window title to something fancy
	window->setTitle(L"FiT AI");

	// load the info
	std::ifstream file;
	file.open("data/FiTAI/info.txt");
	if (!file.is_open())
	{
		cout << "Failed to load data/FiTAI/info.txt" << endl;
		quit();
	}
	float matrix[64];
	for (int i = 0; i < 64; i++)
	{
		file >> matrix[i];
	}
	while (!file.eof())
	{
		std::string type, image;
		int number;
		float channels[8];
		file.ignore(1);
		getline(file, type);
		getline(file, image);
		file >> number;
		for (int i = 0; i < 8; i++)
		{
			file >> channels[i];
		}
		for (int i = 0; i < number; i++)
		{
			Texture* texture = Texture::loadBitmap(image);
			FiTEntity entity;
			entity.type = type;
			entity.texture = texture;
			entity.position = Vector3((float)random.getInteger(-50, 50), 0, (float)random.getInteger(-50, 50));
			for (int i = 0; i < 8; i++)
			{
				entity.channels[i] = channels[i];
			}
			entities.push_back(entity);
		}
	}

	// load the shaders
	shaderScreen = new Cross::Shader("Shaders/Screen.vp", "Shaders/Screen.fp");
	shaderScreen->bindAttribute(0, "vertPosition");
	shaderScreen->bindAttribute(1, "vertTexCoord");
	shaderSimple = new Cross::Shader("Shaders/Simple.vp", "Shaders/Simple.fp");
	shaderSimple->bindAttribute(0, "vertPosition");
	shaderSimple->bindAttribute(1, "vertTexCoord");
	shaderFit1 = new Cross::Shader("Shaders/FiTAI/FiT1.vp", "Shaders/FiTAI/FiT1.fp");
	shaderFit1->bindAttribute(0, "vertData");
	shaderFit1->bindAttribute(1, "vertChannels1");
	shaderFit1->bindAttribute(2, "vertChannels2");
	shaderFit1->begin();
	shaderFit1->setMatrix("orthoMatrix", Matrix::makeOrtho(0, 0, 1, 1));
	shaderFit1->setMatrix("channelsMatrix1", Matrix(matrix[ 0], matrix[ 1], matrix[ 2], matrix[ 3],
												   matrix[ 8], matrix[ 9], matrix[10], matrix[11],
												   matrix[16], matrix[17], matrix[18], matrix[19],
												   matrix[24], matrix[25], matrix[26], matrix[27]));
	shaderFit1->setMatrix("channelsMatrix2", Matrix(matrix[ 4], matrix[ 5], matrix[ 6], matrix[ 7],
												   matrix[12], matrix[13], matrix[14], matrix[15],
												   matrix[20], matrix[21], matrix[22], matrix[23],
												   matrix[28], matrix[29], matrix[30], matrix[31]));
	shaderFit1->setMatrix("channelsMatrix3", Matrix(matrix[32], matrix[33], matrix[34], matrix[35],
												   matrix[40], matrix[41], matrix[42], matrix[43],
												   matrix[48], matrix[49], matrix[50], matrix[51],
												   matrix[56], matrix[57], matrix[58], matrix[59]));
	shaderFit1->setMatrix("channelsMatrix4", Matrix(matrix[36], matrix[37], matrix[38], matrix[39],
												   matrix[44], matrix[45], matrix[46], matrix[47],
												   matrix[52], matrix[53], matrix[54], matrix[55],
												   matrix[60], matrix[61], matrix[62], matrix[63]));
	shaderFit1->end();
	shaderFit2 = new Cross::Shader("Shaders/FiTAI/FiT2.vp", "Shaders/FiTAI/FiT2.fp");
	shaderFit2->bindAttribute(0, "vertData");
	shaderFit2->begin();
	shaderFit2->setMatrix("orthoMatrix", Matrix::makeOrtho(0, 0, 1, 1));
	shaderFit2->setMatrix("channelsMatrix1", Matrix(matrix[ 0], matrix[ 1], matrix[ 2], matrix[ 3],
												   matrix[ 8], matrix[ 9], matrix[10], matrix[11],
												   matrix[16], matrix[17], matrix[18], matrix[19],
												   matrix[24], matrix[25], matrix[26], matrix[27]));
	shaderFit2->setMatrix("channelsMatrix2", Matrix(matrix[ 4], matrix[ 5], matrix[ 6], matrix[ 7],
												   matrix[12], matrix[13], matrix[14], matrix[15],
												   matrix[20], matrix[21], matrix[22], matrix[23],
												   matrix[28], matrix[29], matrix[30], matrix[31]));
	shaderFit2->setMatrix("channelsMatrix3", Matrix(matrix[32], matrix[33], matrix[34], matrix[35],
												   matrix[40], matrix[41], matrix[42], matrix[43],
												   matrix[48], matrix[49], matrix[50], matrix[51],
												   matrix[56], matrix[57], matrix[58], matrix[59]));
	shaderFit2->setMatrix("channelsMatrix4", Matrix(matrix[36], matrix[37], matrix[38], matrix[39],
												   matrix[44], matrix[45], matrix[46], matrix[47],
												   matrix[52], matrix[53], matrix[54], matrix[55],
												   matrix[60], matrix[61], matrix[62], matrix[63]));
	shaderFit2->end();
	shaderFitClean = new Cross::Shader("Shaders/FiTAI/FiTClean.vp", "Shaders/FiTAI/FiTClean.fp");
	shaderFitClean->bindAttribute(0, "vertPosition");
	shaderFitClean->bindAttribute(1, "vertTexCoord");

	// create our quad mesh
	Vector2 meshQuadPositions[4] = { Vector2(0, 0), Vector2(1, 0), Vector2(1, 1), Vector2(0, 1) };
	Vector2 meshQuadTexCoords[4] = { Vector2(0, 0), Vector2(1, 0), Vector2(1, 1), Vector2(0, 1) };
	meshQuad = new Mesh();
	meshQuad->addSubMesh(4, SubMesh::QUADS);
	meshQuad->getSubMesh(0)->addBuffer(meshQuadPositions, 0);
	meshQuad->getSubMesh(0)->addBuffer(meshQuadTexCoords, 1);

	// create the box mesh
	meshBox = Mesh::loadObj("cube2.obj");

	// create the ground mesh
	Vector3 meshGroundPositions[4] = { Vector3(-1, 0, -1), Vector3(-1, 0, 1), Vector3(1, 0, 1), Vector3(1, 0, -1) };
	Vector2 meshGroundTexCoords[4] = { Vector2(1, 0), Vector2(0, 0), Vector2(0, 1), Vector2(1, 1) };
	meshGround = new Mesh();
	meshGround->addSubMesh(4, SubMesh::QUADS);
	meshGround->getSubMesh(0)->addBuffer(meshGroundPositions, 0);
	meshGround->getSubMesh(0)->addBuffer(meshGroundTexCoords, 1);

	// create the influence mesh
	Utility::zeroMemory(meshInfluenceData, 100 * 4 * sizeof(Vector4));
	Utility::zeroMemory(meshInfluence1Channels1, 100 * 4 * sizeof(Vector4));
	Utility::zeroMemory(meshInfluence1Channels2, 100 * 4 * sizeof(Vector4));
	Utility::zeroMemory(meshInfluence2Channels1, 100 * sizeof(Vector4));
	Utility::zeroMemory(meshInfluence2Channels2, 100 * sizeof(Vector4));
	meshInfluence1 = new DynamicMesh();
	meshInfluence1->addSubMesh(100 * 4, SubMesh::QUADS);
	meshInfluence1->getSubMesh(0)->addBuffer(meshInfluenceData, 0);
	meshInfluence1->getSubMesh(0)->addBuffer(meshInfluence1Channels1, 1);
	meshInfluence1->getSubMesh(0)->addBuffer(meshInfluence1Channels2, 2);
	meshInfluence2 = new DynamicMesh();
	meshInfluence2->addSubMesh(100 * 4, SubMesh::QUADS);
	meshInfluence2->getSubMesh(0)->addBuffer(meshInfluenceData, 0);

	// load the textures
	textureBolt = Texture::loadBitmap("bolt.bmp");
	textureSmile = Texture::loadBitmap("smile.bmp");
	textureFalloff1 = Texture::loadBitmap("Images/InfluenceMaps/falloff1.bmp");

	// create the influence maps
	influences = new Cross::RenderTarget(128, 128, 8);
	influenceClean = new Cross::RenderTarget(128, 128);

	// load dafonts
	fontGunplay = Cross::Font::loadFont("Fonts/consola.ttf", 28);

	// create the camera
	camera = new Test::Camera(input);
	camera->setPosition(Vector3(0, 50, 100));
	camera->setDirection(90);
	camera->setPitch(20);

	// setup some gl states
	glShadeModel(GL_SMOOTH);
	glClearColor(0.0f, 0.0f, 0.0f, 1.0f);
	glClearDepth(1.0f);
	glHint(GL_PERSPECTIVE_CORRECTION_HINT, GL_NICEST);
	glEnable(GL_BLEND);
	glBlendFunc(GL_SRC_ALPHA, GL_ONE_MINUS_SRC_ALPHA);
	glEnable(GL_DEPTH_TEST);
	glDepthFunc(GL_LEQUAL);
	glDrawBuffer(GL_BACK);
	glReadBuffer(GL_BACK);
	glCullFace(GL_BACK);
	glEnable(GL_CULL_FACE);
	glEnable(GL_ALPHA_TEST);
	glEnable(GL_TEXTURE_2D);

	// set stuff
	mapToDraw = 0;
	useArrayMethod = true;
	calculateCalculationTime();
}

void FiT::FiTAIGPU::update(float dt)
{
	// update the base app
	Application::update(dt);

	// update the camera
	camera->update(dt);

	// update the entities
	for (std::vector<FiTEntity>::iterator i = entities.begin(); i != entities.end(); i++)
	{
		i->update(dt);
	}

	// swap between methods
	if (input->isKeyPressed(Input::Key::m))
	{
		useArrayMethod = !useArrayMethod;
		calculateCalculationTime();
	}
}

void FiT::FiTAIGPU::draw()
{
	// generate the influence maps
	generateInfluenceMaps();

	// clear the screen to a nice color
	graphics->clear(Color::cornflowerBlue);

	// generate the "clean" map
	influenceClean->begin();
	shaderFitClean->begin();
	shaderFitClean->setMatrix("orthoMatrix", Matrix::makeOrtho(0, 0, 1, 1));
	graphics->clear(Cross::Color::cornflowerBlue);
	shaderFitClean->setTexture("texture", influences->getTexture(mapToDraw));
	meshQuad->draw();
	shaderFitClean->end();
	influenceClean->end();

	// start drawing the world
	glViewport(0, 0, window->getWidth(), window->getHeight());
	glEnable(GL_DEPTH_TEST);
	shaderSimple->begin();
	Matrix view = camera->getView();
	Matrix projection = Matrix::makePerspective(45.0f, window->getWidth() / float(window->getHeight()), 1.0f, 5000.0f);
	Matrix world;
	
	// draw the ground
	world = Matrix::makeScaling(Vector3(50.0f, 0.01f, 50.0f)) * Matrix::makeRotationY(-Math::piOverTwo) * Matrix::makeTranslation(Vector3(0, 0, 0));
	shaderSimple->setMatrix("worldViewProjectionMatrix", world * view * projection);
	shaderSimple->setTexture("texture", influenceClean->getTexture());
	meshGround->draw();

	// draw the allies
	for (std::vector<FiTEntity>::iterator i = entities.begin(); i != entities.end(); i++)
	{
		shaderSimple->setTexture("texture", i->texture);
		world = Matrix::makeScaling(Vector3(2, 2, 2)) * Matrix::makeTranslation(i->position);
		shaderSimple->setMatrix("worldViewProjectionMatrix", world * view * projection);
		meshBox->draw();
	}

	// end drawing the world
	shaderSimple->end();

	for (int i = 0; i < 8; i++)
	{
		// start drawing overlay stuff
		glViewport(window->getWidth() - 64, window->getHeight() - 64 * (i + 1) - (5 * i), 64, 64);
		glDisable(GL_DEPTH_TEST);
		shaderScreen->begin();
		shaderScreen->setMatrix("orthoMatrix", Matrix::makeOrtho(0, 0, 800, 600));

		// draw just a texture for lulz
		shaderScreen->setMatrix("modelMatrix", Matrix::makeScaling(Vector2(800, 600)));
		shaderScreen->setTexture("texture", influences->getTexture(i));
		meshQuad->draw();

		// end drawing overlay stuff
		shaderScreen->end();
	}

	// check if they clicked a different map
	if (input->isMousePressed(0))
	{
		for (int i = 0; i < 8; i++)
		{
			Vector2 mousePos = input->getMousePosition();
			if (mousePos.x > window->getWidth() - 64 && mousePos.y >window->getHeight() - 64 * (i + 1) - (5 * i))
			{
				mousePos.x -= window->getWidth() - 64;
				mousePos.y -= window->getHeight() - 64 * (i + 1) - (5 * i);
				if (mousePos.x < 64 && mousePos.y < 64)
				{
					mapToDraw = i;
				}
			}
		}
	}

	// start drawing overlay stuff
	glViewport(0, 0, window->getWidth(), window->getHeight());
	glDisable(GL_DEPTH_TEST);
	shaderScreen->begin();
	shaderScreen->setMatrix("orthoMatrix", Matrix::makeOrtho(0, 0, 800, 600));

	// draw just a texture for lulz
	std::stringstream ss1(std::stringstream::in | std::stringstream::out);
	ss1 << "Using Uniform Array Method: " << (useArrayMethod ? "Yes" : "No");
	fontGunplay->draw(ss1.str());
	shaderScreen->setMatrix("modelMatrix", Matrix::makeScaling(Vector2(800, 600)));
	shaderScreen->setTexture("texture", fontGunplay->getTexture());
	meshQuad->draw();
	std::stringstream ss2(std::stringstream::in | std::stringstream::out);
	ss2 << "5,000 Calculations: " << timeToCalculate * 1000 << "ms";
	fontGunplay->draw(ss2.str());
	shaderScreen->setMatrix("modelMatrix", Matrix::makeScaling(Vector2(800, 600)) * Matrix::makeTranslation(Vector2(0, -20)));
	shaderScreen->setTexture("texture", fontGunplay->getTexture());
	meshQuad->draw();
	std::stringstream ss3(std::stringstream::in | std::stringstream::out);
	ss3 << "Press (M) to change methods and recalculate.";
	fontGunplay->draw(ss3.str());
	shaderScreen->setMatrix("modelMatrix", Matrix::makeScaling(Vector2(800, 600)) * Matrix::makeTranslation(Vector2(0, -40)));
	shaderScreen->setTexture("texture", fontGunplay->getTexture());
	meshQuad->draw();

	// end drawing overlay stuff
	shaderScreen->end();

	glFlush();
	glFinish();
}

void FiT::FiTAIGPU::uninitialize()
{
	Application::uninitialize();
}

void FiT::FiTAIGPU::generateInfluenceMaps()
{
	if (!useArrayMethod)
	{
		// update the influence buffers
		unsigned int offset = 0;
		for (std::vector<FiTEntity>::iterator i = entities.begin(); i != entities.end(); i++)
		{
			Vector2 position = Vector2(i->position.x, i->position.z) + Vector2(50, 50);
			position /= 100.0f;
			float size = .2f;
			meshInfluenceData[offset + 0] = Vector4(position.x - size, position.y - size, 1, 0.3f);
			meshInfluenceData[offset + 1] = Vector4(position.x + size, position.y - size, 2, 0.3f);
			meshInfluenceData[offset + 2] = Vector4(position.x + size, position.y + size, 3, 0.3f);
			meshInfluenceData[offset + 3] = Vector4(position.x - size, position.y + size, 4, 0.3f);
			for (int j = 0; j < 4; j++)
			{
				meshInfluence1Channels1[offset + j] = Vector4(i->channels[0], i->channels[1], i->channels[2], i->channels[3]);
				meshInfluence1Channels2[offset + j] = Vector4(i->channels[4], i->channels[5], i->channels[6], i->channels[7]);
			}
			offset += 4;
		}
		meshInfluence1->getSubMesh(0)->updateBuffer(meshInfluenceData, 0);
		meshInfluence1->getSubMesh(0)->updateBuffer(meshInfluence1Channels1, 1);
		meshInfluence1->getSubMesh(0)->updateBuffer(meshInfluence1Channels2, 2);

		// bind the render target
		influences->begin();

		// clear the influence map
		graphics->clear(Cross::Color::black);

		// start drawing overlay stuff
		shaderFit1->begin();
		//shaderFit->setMatrix("orthoMatrix", Matrix::makeOrtho(0, 0, 1, 1));

		// update the inf
		shaderFit1->setTexture("texture", textureFalloff1);
		glBlendFunc(GL_SRC_ALPHA, GL_DST_ALPHA);
		meshInfluence1->draw();
		glBlendFunc(GL_SRC_ALPHA, GL_ONE_MINUS_SRC_ALPHA);

		// end drawing overlay stuff
		shaderFit1->end();

		// done rendering the influence
		influences->end();
	}
	else
	{
		// update the influence buffers
		unsigned int offset = 0;
		unsigned index = 0;
		for (std::vector<FiTEntity>::iterator i = entities.begin(); i != entities.end(); i++)
		{
			Vector2 position = Vector2(i->position.x, i->position.z) + Vector2(50, 50);
			position /= 100.0f;
			float size = .2f;
			meshInfluenceData[offset + 0] = Vector4(position.x - size, position.y - size, 1, (float)index);
			meshInfluenceData[offset + 1] = Vector4(position.x + size, position.y - size, 2, (float)index);
			meshInfluenceData[offset + 2] = Vector4(position.x + size, position.y + size, 3, (float)index);
			meshInfluenceData[offset + 3] = Vector4(position.x - size, position.y + size, 4, (float)index);
			meshInfluence2Channels1[index] = Vector4(i->channels[0], i->channels[1], i->channels[2], i->channels[3]);
			meshInfluence2Channels2[index] = Vector4(i->channels[4], i->channels[5], i->channels[6], i->channels[7]);
			offset += 4;
			index++;
		}
		meshInfluence2->getSubMesh(0)->updateBuffer(meshInfluenceData, 0);

		// bind the render target
		influences->begin();

		// clear the influence map
		graphics->clear(Cross::Color::black);

		// start drawing overlay stuff
		shaderFit2->begin();
		//shaderFit->setMatrix("orthoMatrix", Matrix::makeOrtho(0, 0, 1, 1));

		// update the inf
		shaderFit2->setTexture("texture", textureFalloff1);
		shaderFit2->setArray("channels1", meshInfluence2Channels1, 100);
		shaderFit2->setArray("channels2", meshInfluence2Channels2, 100);
		glBlendFunc(GL_SRC_ALPHA, GL_DST_ALPHA);
		meshInfluence2->draw();
		glBlendFunc(GL_SRC_ALPHA, GL_ONE_MINUS_SRC_ALPHA);

		// end drawing overlay stuff
		shaderFit2->end();

		// done rendering the influence
		influences->end();
	}
}

void FiT::FiTAIGPU::calculateCalculationTime()
{
	float start = Time::getCounter();
	for (int i = 0; i < 5000; i++)
	{
		generateInfluenceMaps();
	}
	float end = Time::getCounter();
	timeToCalculate = end - start;
}