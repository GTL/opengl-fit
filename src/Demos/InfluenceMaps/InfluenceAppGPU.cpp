﻿#include <Demos/InfluenceMaps/InfluenceAppGPU.h>
using namespace Cross;

void InfluenceMaps::InfluenceAppGPU::initialize()
{
	// initialize the base app
	Application::initialize();

	// set the window title to something fancy
	window->setTitle(L"Cross Framework - Influence Maps");

	// load the shaders
	shaderScreen = new Cross::Shader("Shaders/Screen.vp", "Shaders/Screen.fp");
	shaderScreen->bindAttribute(0, "vertPosition");
	shaderScreen->bindAttribute(1, "vertTexCoord");
	shaderSimple = new Cross::Shader("Shaders/Simple.vp", "Shaders/Simple.fp");
	shaderSimple->bindAttribute(0, "vertPosition");
	shaderSimple->bindAttribute(1, "vertTexCoord");
	shaderInfluence = new Cross::Shader("Shaders/InfluenceMaps/Influence.vp", "Shaders/InfluenceMaps/Influence.fp");
	shaderInfluence->bindAttribute(0, "vertData");
	shaderComboInfluence = new Cross::Shader("Shaders/InfluenceMaps/ComboInfluence.vp", "Shaders/InfluenceMaps/ComboInfluence.fp");
	shaderComboInfluence->bindAttribute(0, "vertPosition");
	shaderComboInfluence->bindAttribute(1, "vertTexCoord");

	// create our quad mesh
	Vector2 meshQuadPositions[4] = { Vector2(0, 0), Vector2(1, 0), Vector2(1, 1), Vector2(0, 1) };
	Vector2 meshQuadTexCoords[4] = { Vector2(0, 0), Vector2(1, 0), Vector2(1, 1), Vector2(0, 1) };
	meshQuad = new Mesh();
	meshQuad->addSubMesh(4, SubMesh::QUADS);
	meshQuad->getSubMesh(0)->addBuffer(meshQuadPositions, 0);
	meshQuad->getSubMesh(0)->addBuffer(meshQuadTexCoords, 1);

	// create the box mesh
	meshBox = Mesh::loadObj("cube2.obj");

	// create the ground mesh
	Vector3 meshGroundPositions[4] = { Vector3(-1, 0, -1), Vector3(-1, 0, 1), Vector3(1, 0, 1), Vector3(1, 0, -1) };
	Vector2 meshGroundTexCoords[4] = { Vector2(1, 0), Vector2(0, 0), Vector2(0, 1), Vector2(1, 1) };
	meshGround = new Mesh();
	meshGround->addSubMesh(4, SubMesh::QUADS);
	meshGround->getSubMesh(0)->addBuffer(meshGroundPositions, 0);
	meshGround->getSubMesh(0)->addBuffer(meshGroundTexCoords, 1);

	// create the influence mesh
	Utility::zeroMemory(meshInfluenceData, 50 * 4 * sizeof(Vector4));
	meshInfluence = new DynamicMesh();
	meshInfluence->addSubMesh(50 * 4, SubMesh::QUADS);
	meshInfluence->getSubMesh(0)->addBuffer(meshInfluenceData, 0);

	// load the textures
	textureBolt = Texture::loadBitmap("bolt.bmp");
	textureSmile = Texture::loadBitmap("smile.bmp");
	textureFalloff1 = Texture::loadBitmap("Images/InfluenceMaps/falloff1.bmp");

	// create the influence maps
	influenceDanger = new Cross::RenderTarget(128, 128);
	influenceCover = new Cross::RenderTarget(128, 128);
	influenceCombo = new Cross::RenderTarget(128, 128);

	// create the camera
	camera = new Test::Camera(input);
	camera->setPosition(Vector3(0, 50, 100));
	camera->setDirection(90);
	camera->setPitch(20);

	// setup some gl states
	glShadeModel(GL_SMOOTH);
	glClearColor(0.0f, 0.0f, 0.0f, 1.0f);
	glClearDepth(1.0f);
	glHint(GL_PERSPECTIVE_CORRECTION_HINT, GL_NICEST);
	glEnable(GL_BLEND);
	glBlendFunc(GL_SRC_ALPHA, GL_ONE_MINUS_SRC_ALPHA);
	glEnable(GL_DEPTH_TEST);
	glDepthFunc(GL_LEQUAL);
	glDrawBuffer(GL_BACK);
	glReadBuffer(GL_BACK);
	glCullFace(GL_BACK);
	glEnable(GL_CULL_FACE);
	glEnable(GL_ALPHA_TEST);
	glEnable(GL_TEXTURE_2D);

	// create some allies
	for (int i = 0; i < 20; i++)
	{
		allies.push_back(Ally(Vector3((float)random.getInteger(-50, 50), 0, (float)random.getInteger(-50, 50))));
	}

	// create some enemies
	for (int i = 0; i < 20; i++)
	{
		enemies.push_back(Enemy(Vector3((float)random.getInteger(-50, 50), 0, (float)random.getInteger(-50, 50))));
	}
}

void InfluenceMaps::InfluenceAppGPU::update(float dt)
{
	// update the base app
	Application::update(dt);

	// update the camera
	camera->update(dt);
}

void InfluenceMaps::InfluenceAppGPU::draw()
{
	// generate the influence maps
	generateInfluenceMaps();

	// clear the screen to a nice color
	graphics->clear(Color::cornflowerBlue);

	// start drawing the world
	glViewport(0, 0, 800, 600);
	glEnable(GL_DEPTH_TEST);
	shaderSimple->begin();
	Matrix view = camera->getView();
	Matrix projection = Matrix::makePerspective(45.0f, window->getWidth() / float(window->getHeight()), 1.0f, 5000.0f);
	Matrix world;

	// draw the ground
	world = Matrix::makeScaling(Vector3(50.0f, 0.01f, 50.0f)) * Matrix::makeRotationY(-Math::piOverTwo) * Matrix::makeTranslation(Vector3(0, 0, 0));
	shaderSimple->setMatrix("worldViewProjectionMatrix", world * view * projection);
	shaderSimple->setTexture("texture", influenceCombo->getTexture());
	meshGround->draw();

	// draw the allies
	shaderSimple->setTexture("texture", textureSmile);
	for (std::vector<Ally>::iterator i = allies.begin(); i != allies.end(); i++)
	{
		world = Matrix::makeScaling(Vector3(2, 2, 2)) * Matrix::makeTranslation(i->getPosition());
		shaderSimple->setMatrix("worldViewProjectionMatrix", world * view * projection);
		meshBox->draw();
	}

	// draw the enemies
	shaderSimple->setTexture("texture", textureBolt);
	for (std::vector<Enemy>::iterator i = enemies.begin(); i != enemies.end(); i++)
	{
		world = Matrix::makeScaling(Vector3(2, 2, 2)) * Matrix::makeTranslation(i->getPosition());
		shaderSimple->setMatrix("worldViewProjectionMatrix", world * view * projection);
		meshBox->draw();
	}

	// end drawing the world
	shaderSimple->end();

	// start drawing overlay stuff
	glViewport(window->getWidth() - 128, window->getHeight() - 128, 128, 128);
	glDisable(GL_DEPTH_TEST);
	shaderScreen->begin();
	shaderScreen->setMatrix("orthoMatrix", Matrix::makeOrtho(0, 0, 800, 600));

	// draw just a texture for lulz
	shaderScreen->setMatrix("modelMatrix", Matrix::makeScaling(Vector2(800, 600)));
	if (input->isKeyDown(Input::Key::c))
	{
		shaderScreen->setTexture("texture", influenceCombo->getTexture());
	}
	else
	{
		shaderScreen->setTexture("texture", influenceDanger->getTexture());
	}
	meshQuad->draw();

	// end drawing overlay stuff
	shaderScreen->end();
	
	glFlush();
	glFinish();
}

void InfluenceMaps::InfluenceAppGPU::uninitialize()
{
	Application::uninitialize();
}

void InfluenceMaps::InfluenceAppGPU::generateInfluenceMaps()
{
	// update the influence buffers
	unsigned int offset = 0;
	for (std::vector<Ally>::iterator i = allies.begin(); i != allies.end(); i++)
	{
		Vector2 position = Vector2(i->getPosition().x, i->getPosition().z) + Vector2(50, 50);
		position /= 100.0f;
		float size = .2f;
		meshInfluenceData[offset++] = Vector4(position.x - size, position.y - size, 1, 0.3f);
		meshInfluenceData[offset++] = Vector4(position.x + size, position.y - size, 2, 0.3f);
		meshInfluenceData[offset++] = Vector4(position.x + size, position.y + size, 3, 0.3f);
		meshInfluenceData[offset++] = Vector4(position.x - size, position.y + size, 4, 0.3f);
		meshInfluence->getSubMesh(0)->updateBuffer(meshInfluenceData, 0);
	}
	for (std::vector<Enemy>::iterator i = enemies.begin(); i != enemies.end(); i++)
	{
		Vector2 position = Vector2(i->getPosition().x, i->getPosition().z) + Vector2(50, 50);
		position /= 100.0f;
		float size = .2;
		meshInfluenceData[offset++] = Vector4(position.x - size, position.y - size, 1, -0.3f);
		meshInfluenceData[offset++] = Vector4(position.x + size, position.y - size, 2, -0.3f);
		meshInfluenceData[offset++] = Vector4(position.x + size, position.y + size, 3, -0.3f);
		meshInfluenceData[offset++] = Vector4(position.x - size, position.y + size, 4, -0.3f);
		meshInfluence->getSubMesh(0)->updateBuffer(meshInfluenceData, 0);
	}

	// bind the render target
	influenceDanger->begin();

	// clear the influence map
	graphics->clear(Cross::Color::black);

	// start drawing overlay stuff
	shaderInfluence->begin();
	shaderInfluence->setMatrix("orthoMatrix", Matrix::makeOrtho(0, 0, 1, 1));

	// update the inf
	shaderInfluence->setTexture("texture", textureFalloff1);
	glBlendFunc(GL_SRC_ALPHA, GL_DST_ALPHA);
	meshInfluence->draw();
	glBlendFunc(GL_SRC_ALPHA, GL_ONE_MINUS_SRC_ALPHA);

	// end drawing overlay stuff
	shaderInfluence->end();

	// done rendering the influence
	influenceDanger->end();

	// bind the render target
	influenceCover->begin();

	// clear the influence map
	graphics->clear(Cross::Color::black);

	// end drawing overlay stuff
	shaderInfluence->end();

	// done rendering the influence
	influenceCover->end();

	// bind the render target
	influenceCombo->begin();

	// initiate the combo influence shader
	shaderComboInfluence->begin();

	shaderComboInfluence->setMatrix("orthoMatrix", Matrix::makeOrtho(0, 0, 1, 1));

	// clear the combo map
	graphics->clear(Cross::Color::cornflowerBlue);
	
	// set the texture
	shaderComboInfluence->setTexture("texture1", influenceDanger->getTexture());
	shaderComboInfluence->setTexture("texture2", influenceCover->getTexture());

	// draw the quad
	meshQuad->draw();

	// done rendering the combo influence
	shaderComboInfluence->end();

	// finish generating the combo influence
	influenceCombo->end();
}
