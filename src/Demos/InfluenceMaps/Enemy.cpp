#include <Demos/InfluenceMaps/Enemy.h>

InfluenceMaps::Enemy::Enemy(const Cross::Vector3& position)
{
	this->position = position;
}

void InfluenceMaps::Enemy::update(float dt)
{
	// @TODO
}

void InfluenceMaps::Enemy::setPosition(const Cross::Vector3& position)
{
	this->position = position;
}

Cross::Vector3 InfluenceMaps::Enemy::getPosition() const
{
	return position;
}
