#include <Demos/InfluenceMaps/Ally.h>

InfluenceMaps::Ally::Ally(const Cross::Vector3& position)
{
	this->position = position;
}

void InfluenceMaps::Ally::update(float dt)
{
	// @TODO
}

void InfluenceMaps::Ally::setPosition(const Cross::Vector3& position)
{
	this->position = position;
}

Cross::Vector3 InfluenceMaps::Ally::getPosition() const
{
	return position;
}
