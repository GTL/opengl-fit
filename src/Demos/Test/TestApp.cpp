﻿#include <Demos/Test/TestApp.h>
#include <iostream>
using namespace Cross;
using namespace std;

void Test::TestApp::initialize()
{
	Application::initialize();
	window->setTitle(L"ひらがな");
	std::cout << "Hello world!!" << std::endl;
	hue = 0.0f;

	screenShader = new Cross::Shader("Shaders/Screen.vp", "Shaders/Screen.fp");
	screenShader->bindAttribute(0, "vertPosition");
	screenShader->bindAttribute(1, "vertTexCoord");
	simpleShader = new Cross::Shader("Shaders/Simple.vp", "Shaders/Simple.fp");
	simpleShader->bindAttribute(0, "vertPosition");
	simpleShader->bindAttribute(1, "vertTexCoord");

	mesh = new DynamicMesh();
	mesh->addSubMesh(4, SubMesh::QUADS);
	Vector2 verts[4] = {Vector2(0, 0), Vector2(1, 0), Vector2(1, 1), Vector2(0, 1)};
	mesh->getSubMesh(0)->addBuffer(verts, 0);
	Vector2 texs[4] = {Vector2(0, 0), Vector2(1, 0), Vector2(1, 1), Vector2(0, 1)};
	mesh->getSubMesh(0)->addBuffer(texs, 1);

	meshBox = new DynamicMesh();
	SubMesh* meshBoxSub = meshBox->addSubMesh(24, SubMesh::QUADS);
	Vector3 boxVerts[24] = {
		Vector3(-1, 1, -1), Vector3(-1, -1, -1), Vector3(1, -1, -1), Vector3(1, 1, -1),
		Vector3(1, 1, 1), Vector3(1, -1, 1), Vector3(-1, -1, 1), Vector3(-1, 1, 1),
		Vector3(1, 1, -1), Vector3(1, -1, -1), Vector3(1, -1, 1), Vector3(1, 1, 1),
		Vector3(-1, 1, 1), Vector3(-1, -1, 1), Vector3(-1, -1, -1), Vector3(-1, 1, -1),
		Vector3(-1, 1, -1), Vector3(1, 1, -1), Vector3(1, 1, 1), Vector3(-1, 1, 1),
		Vector3(-1, -1, 1), Vector3(1, -1, 1), Vector3(1, -1, -1), Vector3(-1, -1, -1)
	};
	meshBoxSub->addBuffer(boxVerts, 0);
	Vector2 boxTexs[24] = {
		Vector2(1, 0), Vector2(0, 0), Vector2(0, 1), Vector2(1, 1),
		Vector2(0, 0), Vector2(1, 0), Vector2(1, 1), Vector2(0, 1),
		Vector2(0, 0), Vector2(1, 0), Vector2(1, 1), Vector2(0, 1),
		Vector2(0, 0), Vector2(1, 0), Vector2(1, 1), Vector2(0, 1),
		Vector2(0, 0), Vector2(1, 0), Vector2(1, 1), Vector2(0, 1),
		Vector2(0, 0), Vector2(1, 0), Vector2(1, 1), Vector2(0, 1),
	};
	meshBoxSub->addBuffer(boxTexs, 1);
	meshBox = Mesh::loadObj("cube2.obj");
	/*Vector3 meshBoxPositions[6] = { Vector3(-0.5f, -0.5f, -0.5f), Vector3(0.5f, -0.5f, -0.5f), Vector3(0.5f, 0.5f, -0.5f),
									Vector3(-0.5f, -0.5f, 0.5f), Vector3(0.5f, -0.5f, 0.5f), Vector3(0.5f, 0.5f, 0.5f)};
	Vector2 meshBoxTexCoords[6] = { Vector2(0, 0), Vector2(1, 0), Vector2(1, 1),
									Vector2(0, 0), Vector2(1, 0), Vector2(1, 1)};
	meshBox = new Mesh();
	meshBox->addSubMesh(6, SubMesh::TRIANGLES);
	meshBox->getSubMesh(0)->addBuffer(meshBoxPositions, 0);
	meshBox->getSubMesh(0)->addBuffer(meshBoxTexCoords, 1);*/
	
	texture = Cross::Texture::loadBitmap("bolt.bmp");

	camera = new Camera(input);
	
	renderTarget = new RenderTarget(1024, 1024);

	fontGunplay = Font::loadFont("Fonts/gunplay.ttf", 18);


	// setup some gl states
	glShadeModel(GL_SMOOTH);
	glClearColor(0.0f, 0.0f, 0.0f, 1.0f);
	glClearDepth(1.0f);
	glHint(GL_PERSPECTIVE_CORRECTION_HINT, GL_NICEST);
	glEnable(GL_BLEND);
	glBlendFunc(GL_SRC_ALPHA, GL_ONE_MINUS_SRC_ALPHA);
	glEnable(GL_DEPTH_TEST);
	glDrawBuffer(GL_BACK);
	glReadBuffer(GL_BACK);
	glCullFace(GL_BACK);
	glEnable(GL_CULL_FACE);
	glEnable(GL_ALPHA_TEST);
	glEnable(GL_TEXTURE_2D);

	//glDisable(GL_CULL_FACE);
	//glDisable(GL_DEPTH_TEST);
}

void Test::TestApp::update(float dt)
{
	Application::update(dt);
	hue += 1;
	while (hue > 360)
	{
		hue -= 360;
	}
	if (input->isKeyPressed(Input::Key::h))
	{
		hue = 0.0f;
	}
	camera->update(dt);
}

void Test::TestApp::draw()
{
	glViewport(0, 0, 800, 600);
	
	Application::draw();
	
	//graphics->clear(Cross::Color::makeHSV((UInt)hue, 255, 255));
	graphics->clear(Cross::Color::cornflowerBlue);

	/*renderTarget->begin();
	graphics->clear(Cross::Color::paleVioletRed);
	screenShader->begin();
	screenShader->setMatrix("orthoMatrix", Cross::Matrix::makeOrtho(0, 0, 800, 600));
	screenShader->setMatrix("modelMatrix", Cross::Matrix::makeScaling(Vector2(50, 50)) * Cross::Matrix::makeTranslation(input->getMousePosition()));
	cout << (input->getMousePosition() * Cross::Matrix::makeOrtho(0, 0, 800, 600)).x << ", " << (input->getMousePosition() * Cross::Matrix::makeOrtho(0, 0, 800, 600)).y << endl;
	screenShader->setTexture("texture", texture);
	mesh->draw();
	screenShader->end();
	renderTarget->end();

	graphics->clear(Cross::Color::cornflowerBlue);
	screenShader->begin();
	screenShader->setMatrix("orthoMatrix", Cross::Matrix::makeOrtho(0, 0, 800, 600));
	screenShader->setMatrix("modelMatrix", Cross::Matrix::makeScaling(Vector2(700, 600)));
	screenShader->setTexture("texture", renderTarget);
	mesh->draw();
	screenShader->end();*/

	simpleShader->begin();
	Vector2 mousePos = input->getMousePosition();
	mousePos.x = Math::degToRad(mousePos.x);
	mousePos.y = Math::degToRad(mousePos.y);
	Matrix view = camera->getView();//Matrix::makeLookAt(Vector3::normalized(Vector3(Math::cos(mousePos.x), Math::tan(mousePos.y), Math::sin(mousePos.x))) * 20, Vector3(0, 0, 0), Vector3(0, 1, 0));
	Matrix projection = Matrix::makePerspective(45.0f, window->getWidth() / float(window->getHeight()), 1.0f, 5000.0f);
	Matrix world = Matrix::makeScaling(Vector3(5, 5, 5));
	simpleShader->setMatrix("worldViewProjectionMatrix", world * view * projection);
	fontGunplay->draw("Hello world!!");
	simpleShader->setTexture("texture", fontGunplay->getTexture());
	meshBox->draw();
	simpleShader->end();

	glFlush();
}

void Test::TestApp::uninitialize()
{
	Application::uninitialize();
}
