#include <Demos/Test/Camera.h>
using namespace Cross;

Test::Camera::Camera(Cross::Input* input)
{
	this->input = input;

	position = Vector3(0, 0, -15);

	direction = -90.0f;
	pitch = 0.0f;

	view = Matrix::identity;
}

void Test::Camera::update(float dt)
{
	// @TODO fix this little hack...
#if defined(_WIN32) | defined(unix)
	if (input->isMouseDown(Input::leftButton))
	{
#endif
		direction += input->getMouseMove().x / 3.0f;
		pitch -= input->getMouseMove().y / 3.0f;
#if defined(_WIN32) | defined(unix)
	}
#endif

	Vector3 lookTo = Vector3(Math::cos(Math::degToRad(direction)), Math::tan(Math::degToRad(pitch)), Math::sin(Math::degToRad(direction)));
	view = Matrix::makeLookAt(position, position + lookTo, Vector3(0, 1, 0));

	if (input->isKeyDown(Input::Key::w))
	{
		position += getForward() * 50 * dt;
	}
	if (input->isKeyDown(Input::Key::s))
	{
		position -= getForward() * 50 * dt;
	}
	if (input->isKeyDown(Input::Key::a))
	{
		position -= getRight() * 50 * dt;
	}
	if (input->isKeyDown(Input::Key::d))
	{
		position += getRight() * 50 * dt;
	}
	if (input->isKeyDown(Input::Key::space))
	{
		position += getUp() * 50 * dt;
	}
	if (input->isKeyDown(Input::Key::leftShift))
	{
		position -= getUp() * 50 * dt;
	}
}

Matrix Test::Camera::getView() const
{
	return view;
}

void Test::Camera::setPosition(const Cross::Vector3& position)
{
	this->position = position;
}

Cross::Vector3 Test::Camera::getPosition() const
{
	return position;
}

void Test::Camera::setDirection(float direction)
{
	this->direction = direction;
}

float Test::Camera::getDirection() const
{
	return direction;
}

void Test::Camera::setPitch(float pitch)
{
	this->pitch = pitch;
}

float Test::Camera::getPitch() const
{
	return pitch;
}

Cross::Vector3 Test::Camera::getForward() const
{
	return Vector3(view.m[0][2], view.m[1][2], view.m[2][2]);
}

Cross::Vector3 Test::Camera::getUp() const
{
	return Vector3(view.m[0][1], view.m[1][1], view.m[2][1]);
}

Cross::Vector3 Test::Camera::getRight() const
{
	return Vector3(view.m[0][0], view.m[1][0], view.m[2][0]);
}
