#include <Cross/Application.h>
#include <Cross/Window.h>
#import "OpenGLView.h"
#import "CrossApp.h"

OpenGLView* __open_gl_view;

@implementation OpenGLView

+ (NSOpenGLPixelFormat*) defaultPixelFormat
{
	NSOpenGLPixelFormatAttribute attributes[] = {
		NSOpenGLPFAWindow,
		NSOpenGLPFADoubleBuffer,
		NSOpenGLPFADepthSize, (NSOpenGLPixelFormatAttribute)24,
		(NSOpenGLPixelFormatAttribute)nil
	};
	return [[[NSOpenGLPixelFormat alloc] initWithAttributes:attributes] autorelease];
}

- (void) idle:(NSTimer*)pTimer
{
	Cross::Application::getApplication()->update(0.01f);
	Cross::Application::getApplication()->draw();
	[[self openGLContext] flushBuffer];
}

- (void) prepareOpenGL
{
	pTimer = [NSTimer timerWithTimeInterval:(1.0 / 60.0) target:self selector:@selector(idle:) userInfo:nil repeats:YES];
	[[NSRunLoop currentRunLoop]addTimer:pTimer forMode: NSDefaultRunLoopMode];
	__open_gl_view = self;
}

@end