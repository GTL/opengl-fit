#include <Demos/Test/TestApp.h>
#include <Demos/InfluenceMaps/InfluenceAppGPU.h>
#include <Demos/FiTAI/FiTAIGPU.h>

int main()
{
	//Test::TestApp test;
	//InfluenceMaps::InfluenceAppGPU test;
	FiT::FiTAIGPU test;
	test.run();
}

#if defined(_WIN32)
int CALLBACK WinMain(HINSTANCE hInstance, HINSTANCE hPrevInstance, LPSTR lpCmdLine,int nCmdShow)
{
	return main();
}
#endif