#include <Cross/Window.h>
#include <iostream>

Cross::Window::Window(void* handle)
{
	window = (CrossWindow*)handle;
	[window setCrossWindow:this];
}

Cross::Window::~Window()
{
}

void Cross::Window::update()
{
}

void Cross::Window::setTitle(const std::wstring& title)
{
	NSString* str = [[NSString alloc] initWithBytes:title.c_str() length:(title.length() * 4) encoding:NSUTF32LittleEndianStringEncoding];
	[window setTitle:str];
	[str release];
}

std::wstring Cross::Window::getTitle()
{
	return L""; // @TODO
}

void Cross::Window::setWidth(UInt width)
{
	NSRect resize;
	resize.origin.x = 0; resize.origin.y = 0;
	resize.size.width = 0; resize.size.height = 0;
	resize = [(id)window frameRectForContentRect:resize styleMask:NSTitledWindowMask];
	NSRect windowFrame = [window frame];
	windowFrame.size.width = width + resize.size.width;
	[window setFrame:windowFrame display:YES];
}

void Cross::Window::setHeight(UInt height)
{
	NSRect resize;
	resize.origin.x = 0; resize.origin.y = 0;
	resize.size.width = 0; resize.size.height = 0;
	resize = [(id)window frameRectForContentRect:resize styleMask:NSTitledWindowMask];
	NSLog(@"%f, %f, %f, %f", resize.origin.x, resize.origin.y, resize.size.width, resize.size.height);
	NSRect windowFrame = [window frame];
	windowFrame.size.height = height + resize.size.height;
	[window setFrame:windowFrame display:YES];
}

void Cross::Window::setSize(UInt width, UInt height)
{
	NSRect resize;
	resize.origin.x = 0; resize.origin.y = 0;
	resize.size.width = 0; resize.size.height = 0;
	resize = [(id)window frameRectForContentRect:resize styleMask:NSTitledWindowMask];
	NSRect windowFrame = [window frame];
	windowFrame.size.width = width + resize.size.width;
	windowFrame.size.height = height + resize.size.height;
	[window setFrame:windowFrame display:YES];
}

UInt Cross::Window::getWidth() const
{
	NSRect resize;
	resize.origin.x = 0; resize.origin.y = 0;
	resize.size.width = 0; resize.size.height = 0;
	resize = [(id)window frameRectForContentRect:resize styleMask:NSTitledWindowMask];
	return [window frame].size.width - resize.size.width;
}

UInt Cross::Window::getHeight() const
{
	NSRect resize;
	resize.origin.x = 0; resize.origin.y = 0;
	resize.size.width = 0; resize.size.height = 0;
	resize = [(id)window frameRectForContentRect:resize styleMask:NSTitledWindowMask];
	return [window frame].size.height - resize.size.height;
}

void Cross::Window::getSize(UInt* width, UInt* height)
{
	NSRect resize;
	resize.origin.x = 0; resize.origin.y = 0;
	resize.size.width = 0; resize.size.height = 0;
	resize = [(id)window frameRectForContentRect:resize styleMask:NSTitledWindowMask];
	*width = [window frame].size.width - resize.size.width;
	*height = [window frame].size.height - resize.size.height;
}

void Cross::Window::hookKeyDown(void(*func)(const KeyEvent&, void*), void* data)
{
	keyDown.hook(func, data);
}

void Cross::Window::callKeyDown(const KeyEvent& event)
{
	keyDown.call(event);
}

void Cross::Window::hookKeyUp(void(*func)(const KeyEvent&, void*), void* data)
{
	keyUp.hook(func, data);
}

void Cross::Window::callKeyUp(const KeyEvent& event)
{
	keyUp.call(event);
}

void Cross::Window::hookMouseMove(void(*func)(const MouseMoveEvent&, void*), void* data)
{
	mouseMove.hook(func, data);
}

void Cross::Window::callMouseMove(const MouseMoveEvent& event)
{
	mouseMove.call(event);
}

void Cross::Window::hookMouseDown(void(*func)(const MouseButtonEvent&, void*), void* data)
{
	mouseDown.hook(func, data);
}

void Cross::Window::callMouseDown(const MouseButtonEvent& event)
{
	mouseDown.call(event);
}

void Cross::Window::hookMouseUp(void(*func)(const MouseButtonEvent&, void*), void* data)
{
	mouseUp.hook(func, data);
}

void Cross::Window::callMouseUp(const MouseButtonEvent& event)
{
	mouseUp.call(event);
}