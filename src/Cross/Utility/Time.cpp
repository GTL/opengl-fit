#include <Cross/Utility/Time.h>
#if defined(unix)
#include <sys/time.h>
#endif

float Cross::Time::getCounter()
{
	static bool first = true;
#if defined(_WIN32)
	static __int64 start;
	__int64 frequency, counter;
	QueryPerformanceFrequency((LARGE_INTEGER*)&frequency);
	QueryPerformanceCounter((LARGE_INTEGER*)&counter);
	if (first)
	{
		start = counter;
		first = false;
	}
	return (counter - start) / float(frequency);
#elif defined(unix)
	static __time_t start;
	timeval time;
	gettimeofday(&time, nullptr);
	if (first)
	{
		start = time.tv_sec;
		first = false;
	}
	return (time.tv_sec - start) + (time.tv_usec / 1000000.0f);
#endif
}
