#include <Cross/Utility/Utility.h>

void Cross::Utility::zeroMemory(void* source, unsigned int size)
{
	for (unsigned int i = 0; i < size; i++)
	{
		((unsigned char*)source)[i] = 0;
	}
}