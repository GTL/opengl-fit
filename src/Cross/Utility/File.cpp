#include <Cross/Utility/File.h>
#include <fstream>

bool Cross::File::getText(const std::string& fileName, std::string& result)
{
	std::ifstream file(fileName.c_str());
	if (!file.is_open())
	{
		return false;
	}
	std::string line;
	while (!file.eof())
	{
		getline(file, line);
		result += line + "\n";
	}
	result = result.substr(0, result.length() - 1);
	file.close();
    return true;
}

bool Cross::File::getData(const std::string& fileName, Byte** data, UInt* size)
{
	std::ifstream file;
	file.open(fileName.c_str(), std::ios::binary);
	if (!file.is_open())
	{
		return false;
	}
	file.seekg(0, std::ios::end);
	int length = (int)file.tellg();
	file.seekg(0, std::ios::beg);
	*data = new Byte[length];
	file.read((char*)*data, length);
	if (size != nullptr)
	{
		*size = length;
	}
    return true;
}