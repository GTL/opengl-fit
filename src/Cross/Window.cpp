#include <Cross/Window.h>
#include <Cross/Application.h>
#if defined(unix)
#include <iconv.h>
#include <errno.h>
#endif

// @TODO remove this
#include <iostream>
using namespace std;

#if defined(_WIN32)
std::map<HWND, Cross::Window*> Cross::Window::handleMap;

LRESULT CALLBACK Cross::Window::windowProcedure(HWND window, UINT message, WPARAM wParam, LPARAM lParam)
{
	switch (message)
	{
	case WM_CLOSE:
		DestroyWindow(window);
		handleMap[window]->handle = false;
		return 0;
	/*case WM_CHAR:
		switch (wParam)
		{
		case VK_RETURN:
			break;
		case VK_BACK:
			if (handleMap[window]->keyboardString.size() != 0)
			{
				handleMap[window]->keyboardString.erase(handleMap[window]->keyboardString.begin() + handleMap[window]->keyboardString.size() - 1);
			}
			break;
		default:
			handleMap[window]->keyboardString += wParam;
		}
		break;*/
	case WM_KEYDOWN:
		{
			KeyEvent event;
			event.key = wParam;
			handleMap[window]->callKeyDown(event);
		}
		break;
	case WM_KEYUP:
		{
			KeyEvent event;
			event.key = wParam;
			handleMap[window]->callKeyUp(event);
		}
		break;
	case WM_LBUTTONDOWN:
		{
			MouseButtonEvent event;
			event.button = 0;
			handleMap[window]->callMouseDown(event);
		}
		break;
	case WM_LBUTTONUP:
		{
			MouseButtonEvent event;
			event.button = 0;
			handleMap[window]->callMouseUp(event);
		}
		break;
	case WM_RBUTTONDOWN:
		{
			MouseButtonEvent event;
			event.button = 1;
			handleMap[window]->callMouseDown(event);
		}
		break;
	case WM_RBUTTONUP:
		{
			MouseButtonEvent event;
			event.button = 1;
			handleMap[window]->callMouseUp(event);
		}
		break;
	case WM_MBUTTONDOWN:
		{
			MouseButtonEvent event;
			event.button = 2;
			handleMap[window]->callMouseDown(event);
		}
		break;
	case WM_MBUTTONUP:
		{
			MouseButtonEvent event;
			event.button = 2;
			handleMap[window]->callMouseUp(event);
		}
		break;
	case WM_DESTROY:
		Cross::Application::quit();
		return 0;
	}
	return DefWindowProc(window, message, wParam, lParam);
}

Cross::Window::Window(HWND handle, DWORD style)
{
	handleMap.insert(std::make_pair(handle, this));
	this->handle = handle;
	this->style = style;
}
#elif defined(unix)
Cross::Window::Window(::Window handle, Display* display, XVisualInfo* visual, GLXFBConfig* config)
{
	this->handle = handle;
	this->display = display;
	this->visual = visual;
	this->config = config;
}
#endif

Cross::Window::~Window()
{
	#if defined(_WIN32)
	handleMap.erase(handleMap.find(handle));
	#endif
}

void Cross::Window::update()
{
	#if defined(_WIN32)
	static MSG msg;
	msg.message = WM_NULL;
	while (PeekMessage(&msg, handle, 0, 0, PM_REMOVE))
	{
		TranslateMessage(&msg);
		DispatchMessage(&msg);
	}
	#elif defined(unix)
	events.clear();
	while (XPending(display))
	{
		XEvent event;
		XNextEvent(display, &event);
		events.push_back(event);
	}
	for (unsigned int i = 0; i < events.size(); i++)
	{
		XEvent& windowEvent = events[i];
		bool first = (i == 0);
		bool last = (i == events.size() - 1);
		switch (windowEvent.type)
		{
		case 33:
			XDestroyWindow(display, handle);
			break;
		case DestroyNotify:
			handle = nullptr;
			display = nullptr;
			break;
		case ButtonPress:
		{
			std::cout << windowEvent.xbutton.button << std::endl;
			MouseButtonEvent event;
			switch (windowEvent.xbutton.button)
			{
			case 1:
				event.button = 0;
				break;
			case 2:
				event.button = 3;
				break;
			case 3:
				event.button = 1;
				break;
			}
			callMouseDown(event);
			break;
		}
		case ButtonRelease:
		{
			std::cout << windowEvent.xbutton.button << std::endl;
			MouseButtonEvent event;
			switch (windowEvent.xbutton.button)
			{
			case 1:
				event.button = 0;
				break;
			case 2:
				event.button = 3;
				break;
			case 3:
				event.button = 1;
				break;
			}
			callMouseUp(event);
			break;
		}
		case KeyPress:
		{
			KeyEvent event;
			event.key = windowEvent.xbutton.button;
			callKeyDown(event);
			break;
		}
		case KeyRelease:
			if (last || events[i + 1].type != KeyPress)
			{
				KeyEvent event;
				event.key = windowEvent.xbutton.button;
				callKeyUp(event);
			}
			break;
		}
	}
	#endif
}

#if defined(_WIN32)
HWND Cross::Window::getHandle()
{
	return handle;
}
#endif

void Cross::Window::setTitle(const std::wstring& title)
{
	#if defined(_WIN32)
	SetWindowText(handle, title.c_str());
	#elif defined(unix)
	wchar_t* wcstr = new wchar_t[title.length()];
	for (unsigned int i = 0; i < title.length(); i++)
	{
		wcstr[i] = title[i];
	}
	char* buffer = new char[100];
	unsigned int size = 100;
	size_t inleft = title.length() * sizeof(wchar_t);
	size_t outleft = size;
	iconv_t cd = iconv_open("UTF-8", "WCHAR_T");
	const void* output = buffer;
	iconv(cd, (char**)&wcstr, &inleft, (char**)&output, &outleft);
	iconv_close(cd);
	size = size - outleft;
	XChangeProperty(display, handle, XInternAtom(display, "_NET_WM_NAME", false), XInternAtom(display, "UTF8_STRING", false), 8, PropModeReplace, (unsigned char*)buffer, size);
	//delete[] wcstr; // @TODO figure out why this crashes...
	delete[] buffer;
	#endif
}

std::wstring Cross::Window::getTitle()
{
	#if defined(_WIN32)
	wchar_t buffer[256];
	GetWindowText(handle, buffer, 256);
	return std::wstring(buffer);
	#elif defined(unix)
	// @TODO
	#endif
}

void Cross::Window::setWidth(UInt width)
{
#if defined(_WIN32)
	RECT rect = {0, 0, width, getHeight()};
	AdjustWindowRect(&rect, style, false);
	SetWindowPos(handle, nullptr, 0, 0, rect.right - rect.left, rect.bottom - rect.top, SWP_NOMOVE | SWP_NOZORDER);
#elif defined(unix)
	// @TODO
#endif
}

void Cross::Window::setHeight(UInt height)
{
#if defined(_WIN32)
	RECT rect = {0, 0, getWidth(), height};
	AdjustWindowRect(&rect, style, false);
	SetWindowPos(handle, nullptr, 0, 0, rect.right - rect.left, rect.bottom - rect.top, SWP_NOMOVE | SWP_NOZORDER);
#elif defined(unix)
	// @TODO
#endif
}

void Cross::Window::setSize(UInt width, UInt height)
{
#if defined(_WIN32)
	RECT rect = {0, 0, width, height};
	AdjustWindowRect(&rect, style, false);
	SetWindowPos(handle, nullptr, 0, 0, rect.right - rect.left, rect.bottom - rect.top, SWP_NOMOVE | SWP_NOZORDER);
#elif defined(unix)
	// @TODO
#endif
}

UInt Cross::Window::getWidth() const
{
#if defined(_WIN32)
	RECT rect = {0, 0, 0, 0};
	AdjustWindowRect(&rect, style, false);
	int borders = -rect.left + rect.right;
	GetWindowRect(handle, &rect);
	return rect.right - rect.left - borders;
#elif defined(unix)
	XWindowAttributes attributes;
	XGetWindowAttributes(display, handle, &attributes);
	return attributes.width;
#endif
}

UInt Cross::Window::getHeight() const
{
#if defined(_WIN32)
	RECT rect = {0, 0, 0, 0};
	AdjustWindowRect(&rect, style, false);
	int borders = -rect.top + rect.bottom;
	GetWindowRect(handle, &rect);
	return rect.bottom - rect.top - borders;
#elif defined(unix)
	XWindowAttributes attributes;
	XGetWindowAttributes(display, handle, &attributes);
	return attributes.height;
#endif
}

void Cross::Window::getSize(UInt* width, UInt* height)
{
#if defined(_WIN32)
	RECT rect = {0, 0, 0, 0};
	AdjustWindowRect(&rect, style, false);
	int borders1 = -rect.left + rect.right;
	int borders2 = -rect.top + rect.bottom;
	GetWindowRect(handle, &rect);
	*width = rect.right - rect.left - borders1;
	*height = rect.bottom - rect.top - borders2;
#elif defined(unix)
	XWindowAttributes attributes;
	XGetWindowAttributes(display, handle, &attributes);
	*width = attributes.width;
	*height = attributes.height;
#endif
}

void Cross::Window::hookKeyDown(void(*func)(const KeyEvent&, void*), void* data)
{
	keyDown.hook(func, data);
}

void Cross::Window::callKeyDown(const KeyEvent& event)
{
	keyDown.call(event);
}

void Cross::Window::hookKeyUp(void(*func)(const KeyEvent&, void*), void* data)
{
	keyUp.hook(func, data);
}

void Cross::Window::callKeyUp(const KeyEvent& event)
{
	keyUp.call(event);
}

void Cross::Window::hookMouseMove(void(*func)(const MouseMoveEvent&, void*), void* data)
{
	mouseMove.hook(func, data);
}

void Cross::Window::callMouseMove(const MouseMoveEvent& event)
{
	mouseMove.call(event);
}

void Cross::Window::hookMouseDown(void(*func)(const MouseButtonEvent&, void*), void* data)
{
	mouseDown.hook(func, data);
}

void Cross::Window::callMouseDown(const MouseButtonEvent& event)
{
	mouseDown.call(event);
}

void Cross::Window::hookMouseUp(void(*func)(const MouseButtonEvent&, void*), void* data)
{
	mouseUp.hook(func, data);
}

void Cross::Window::callMouseUp(const MouseButtonEvent& event)
{
	mouseUp.call(event);
}
