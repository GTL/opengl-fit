#include <Cross/Input/Input.h>

#if defined(_WIN32)
Cross::Input::Key Cross::Input::Key::a('A'); Cross::Input::Key Cross::Input::Key::b('B');
Cross::Input::Key Cross::Input::Key::c('C'); Cross::Input::Key Cross::Input::Key::d('D');
Cross::Input::Key Cross::Input::Key::e('E'); Cross::Input::Key Cross::Input::Key::f('F');
Cross::Input::Key Cross::Input::Key::g('G'); Cross::Input::Key Cross::Input::Key::h('H');
Cross::Input::Key Cross::Input::Key::i('I'); Cross::Input::Key Cross::Input::Key::j('J');
Cross::Input::Key Cross::Input::Key::k('K'); Cross::Input::Key Cross::Input::Key::l('L');
Cross::Input::Key Cross::Input::Key::m('M'); Cross::Input::Key Cross::Input::Key::n('N');
Cross::Input::Key Cross::Input::Key::o('O'); Cross::Input::Key Cross::Input::Key::p('P');
Cross::Input::Key Cross::Input::Key::q('Q'); Cross::Input::Key Cross::Input::Key::r('R');
Cross::Input::Key Cross::Input::Key::s('S'); Cross::Input::Key Cross::Input::Key::t('T');
Cross::Input::Key Cross::Input::Key::u('U'); Cross::Input::Key Cross::Input::Key::v('V');
Cross::Input::Key Cross::Input::Key::w('W'); Cross::Input::Key Cross::Input::Key::x('X');
Cross::Input::Key Cross::Input::Key::y('Y'); Cross::Input::Key Cross::Input::Key::z('Z');
Cross::Input::Key Cross::Input::Key::space(VK_SPACE);
Cross::Input::Key Cross::Input::Key::leftShift(VK_SHIFT);
Cross::Input::Key Cross::Input::Key::up(VK_UP);
Cross::Input::Key Cross::Input::Key::down(VK_DOWN);
Cross::Input::Key Cross::Input::Key::left(VK_LEFT);
Cross::Input::Key Cross::Input::Key::right(VK_RIGHT);
#elif defined(unix)
Cross::Input::Key Cross::Input::Key::a(38); Cross::Input::Key Cross::Input::Key::b(56);
Cross::Input::Key Cross::Input::Key::c(54); Cross::Input::Key Cross::Input::Key::d(40);
Cross::Input::Key Cross::Input::Key::e(26); Cross::Input::Key Cross::Input::Key::f(41);
Cross::Input::Key Cross::Input::Key::g(42); Cross::Input::Key Cross::Input::Key::h(43);
Cross::Input::Key Cross::Input::Key::i(31); Cross::Input::Key Cross::Input::Key::j(44);
Cross::Input::Key Cross::Input::Key::k(45); Cross::Input::Key Cross::Input::Key::l(46);
Cross::Input::Key Cross::Input::Key::m(58); Cross::Input::Key Cross::Input::Key::n(57);
Cross::Input::Key Cross::Input::Key::o(32); Cross::Input::Key Cross::Input::Key::p(33);
Cross::Input::Key Cross::Input::Key::q(24); Cross::Input::Key Cross::Input::Key::r(27);
Cross::Input::Key Cross::Input::Key::s(39); Cross::Input::Key Cross::Input::Key::t(28);
Cross::Input::Key Cross::Input::Key::u(30); Cross::Input::Key Cross::Input::Key::v(55);
Cross::Input::Key Cross::Input::Key::w(25); Cross::Input::Key Cross::Input::Key::x(53);
Cross::Input::Key Cross::Input::Key::y(29); Cross::Input::Key Cross::Input::Key::z(52);
Cross::Input::Key Cross::Input::Key::space(65);
Cross::Input::Key Cross::Input::Key::leftShift(50);
#elif defined(__APPLE__)
Cross::Input::Key Cross::Input::Key::a( 0); Cross::Input::Key Cross::Input::Key::b(18);
Cross::Input::Key Cross::Input::Key::c( 8); Cross::Input::Key Cross::Input::Key::d( 2);
Cross::Input::Key Cross::Input::Key::e(14); Cross::Input::Key Cross::Input::Key::f( 3);
Cross::Input::Key Cross::Input::Key::g( 5); Cross::Input::Key Cross::Input::Key::h( 4);
Cross::Input::Key Cross::Input::Key::i(34); Cross::Input::Key Cross::Input::Key::j(38);
Cross::Input::Key Cross::Input::Key::k(40); Cross::Input::Key Cross::Input::Key::l(37);
Cross::Input::Key Cross::Input::Key::m(46); Cross::Input::Key Cross::Input::Key::n(45);
Cross::Input::Key Cross::Input::Key::o(31); Cross::Input::Key Cross::Input::Key::p(35);
Cross::Input::Key Cross::Input::Key::q(12); Cross::Input::Key Cross::Input::Key::r(15);
Cross::Input::Key Cross::Input::Key::s(1); Cross::Input::Key Cross::Input::Key::t(17);
Cross::Input::Key Cross::Input::Key::u(32); Cross::Input::Key Cross::Input::Key::v(9);
Cross::Input::Key Cross::Input::Key::w(13); Cross::Input::Key Cross::Input::Key::x(7);
Cross::Input::Key Cross::Input::Key::y(16); Cross::Input::Key Cross::Input::Key::z(6);
Cross::Input::Key Cross::Input::Key::space(49);
Cross::Input::Key Cross::Input::Key::leftShift(56);
Cross::Input::Key Cross::Input::Key::up(126);
Cross::Input::Key Cross::Input::Key::down(125);
Cross::Input::Key Cross::Input::Key::left(123);
Cross::Input::Key Cross::Input::Key::right(124);
#endif

Byte Cross::Input::leftButton = 0;
Byte Cross::Input::rightButton = 1;
Byte Cross::Input::middleButton = 2;

Cross::Input::Key::Key(Int key)
{
	this->key = key;
}

void Cross::Input::KeyDown(const Window::KeyEvent& event, void* data)
{
	((Input*)data)->keyDown[event.key] = true;
}

void Cross::Input::KeyUp(const Window::KeyEvent& event, void* data)
{
	((Input*)data)->keyDown[event.key] = false;
}

void Cross::Input::MouseMove(const Window::MouseMoveEvent& event, void* data)
{
	((Input*)data)->mousePosition.x = (float)event.x;
	((Input*)data)->mousePosition.y = (float)event.y;
}

void Cross::Input::MouseDown(const Window::MouseButtonEvent& event, void* data)
{
	((Input*)data)->mouseDown[event.button] = true;
}

void Cross::Input::MouseUp(const Window::MouseButtonEvent& event, void* data)
{
	((Input*)data)->mouseDown[event.button] = false;
}

void Cross::Input::calculateMousePosition()
{
#if defined(_WIN32)
	POINT mousePos;
	GetCursorPos(&mousePos);

	// account for window position and style
	RECT windowRect;
	RECT style;
	windowRect.bottom = windowRect.left = windowRect.right = windowRect.top = 0;
	style.bottom = style.left = style.right = style.top = 0;
	GetWindowRect(window->handle, &windowRect);
	AdjustWindowRect(&style, window->style, false);
	mousePosition.x = float(mousePos.x - (windowRect.left - style.left));
	mousePosition.y = float(mousePos.y - (windowRect.top - style.top));
	mousePosition.y = window->getHeight() - mousePosition.y;
#elif defined(unix)
	prevMousePosition = mousePosition;
	int tempInt, mouseX, mouseY;
	::Window tempWindow;
	unsigned int tempUInt;
	XQueryPointer(window->display, window->handle, &tempWindow, &tempWindow, &tempInt, &tempInt, &mouseX, &mouseY, &tempUInt);
	mousePosition = Vector2(mouseX, mouseY);
	mousePosition.y = window->getHeight() - mousePosition.y;
#elif defined(__APPLE__)
#endif
}

Cross::Input::Input(Window* window)
{
	this->window = window;
	window->hookKeyDown(KeyDown, this);
	window->hookKeyUp(KeyUp, this);
#if defined(unix) | defined(__APPLE__)
	window->hookMouseMove(MouseMove, this);
#endif
	window->hookMouseDown(MouseDown, this);
	window->hookMouseUp(MouseUp, this);
	
	for (int i = 0; i < 256; i++)
	{
		keyDown[i] = false;
		previousKeyDown[i] = false;
	}

	for (int i = 0; i < 3; i++)
	{
		mouseDown[i] = false;
		previousMouseDown[i] = false;
	}

	update();
}

void Cross::Input::update()
{
	for (int i = 0; i < 256; i++)
	{
		previousKeyDown[i] = keyDown[i];
	}
	for (int i = 0; i < 3; i++)
	{
		previousMouseDown[i] = mouseDown[i];
	}
	
	calculateMousePosition();

	mouseMove.x = mouseMove.y = 0;
	if (prevMousePosition.x != mousePosition.x || prevMousePosition.y != mousePosition.y)
	{
		Window::MouseMoveEvent event;
		event.x = (UInt)mousePosition.x;
		event.y = (UInt)mousePosition.y;
		window->callMouseMove(event);
		mouseMove = mousePosition - prevMousePosition;
		prevMousePosition = mousePosition;
	}
}

bool Cross::Input::isKeyDown(Key key)
{
	return keyDown[key.key];
}

bool Cross::Input::isKeyPressed(Key key)
{
	return (keyDown[key.key] && !previousKeyDown[key.key]);
}

bool Cross::Input::isKeyReleased(Key key)
{
	return (!keyDown[key.key] && previousKeyDown[key.key]);
}

Cross::Vector2 Cross::Input::getMousePosition()
{
	calculateMousePosition();
	return mousePosition;
}

Cross::Vector2 Cross::Input::getMouseMove() const
{
	return mouseMove;
}

bool Cross::Input::isMouseDown(Byte button)
{
	return mouseDown[button];
}

bool Cross::Input::isMousePressed(Byte button)
{
	return (mouseDown[button] && !previousMouseDown[button]);
}

bool Cross::Input::isMouseReleased(Byte button)
{
	return (!mouseDown[button] && previousMouseDown[button]);
}
