#include <Cross/Graphics/DynamicSubMesh.h>

Cross::DynamicSubMesh::DynamicSubMesh(UInt vertexCount, Type type) : SubMesh(vertexCount, type)
{
}

void Cross::DynamicSubMesh::addBuffer(Vector2* data, UInt index)
{
	// bind the mesh so that we can modify it
#if defined(__APPLE__)
	glBindVertexArrayAPPLE((vertexArray));
#else
	glBindVertexArray(vertexArray);
#endif

	// generate a new vertex buffer
	UInt object;
	glGenBuffers(1, &object);

	// bind the vertex buffer
	glBindBuffer(GL_ARRAY_BUFFER, object);

	// set the data of this buffer
	glBufferData(GL_ARRAY_BUFFER, (vertexCount * 2) * sizeof(GLfloat), (float*)data, GL_STATIC_DRAW);

	// setup the attribute pointer to whatever was passed in
	glVertexAttribPointer((GLuint)index, 2, GL_FLOAT, GL_FALSE, 0, 0);

	// set the attribute index
	glEnableVertexAttribArray(index);

	// store this in our map
	vertexBuffers.insert(std::make_pair(index, object));

	// we done here, unbind the mesh
#if defined(__APPLE__)
	glBindVertexArrayAPPLE((0));
#else
	glBindVertexArray(0);
#endif
}

void Cross::DynamicSubMesh::addBuffer(Vector3* data, UInt index)
{
	// bind the mesh so that we can modify it
#if defined(__APPLE__)
	glBindVertexArrayAPPLE((vertexArray));
#else
	glBindVertexArray(vertexArray);
#endif

	// generate a new vertex buffer
	UInt object;
	glGenBuffers(1, &object);

	// bind the vertex buffer
	glBindBuffer(GL_ARRAY_BUFFER, object);

	// set the data of this buffer
	glBufferData(GL_ARRAY_BUFFER, (vertexCount * 3) * sizeof(GLfloat), (float*)data, GL_STATIC_DRAW);

	// setup the attribute pointer to whatever was passed in
	glVertexAttribPointer((GLuint)index, 3, GL_FLOAT, GL_FALSE, 0, 0);

	// set the attribute index
	glEnableVertexAttribArray(index);

	// store this in our map
	vertexBuffers.insert(std::make_pair(index, object));

	// we done here, unbind the mesh
#if defined(__APPLE__)
	glBindVertexArrayAPPLE((0));
#else
	glBindVertexArray(0);
#endif
}

void Cross::DynamicSubMesh::addBuffer(Vector4* data, UInt index)
{
	// bind the mesh so that we can modify it
#if defined(__APPLE__)
	glBindVertexArrayAPPLE((vertexArray));
#else
	glBindVertexArray(vertexArray);
#endif

	// generate a new vertex buffer
	UInt object;
	glGenBuffers(1, &object);

	// bind the vertex buffer
	glBindBuffer(GL_ARRAY_BUFFER, object);

	// set the data of this buffer
	glBufferData(GL_ARRAY_BUFFER, (vertexCount * 4) * sizeof(GLfloat), (float*)data, GL_STATIC_DRAW);

	// setup the attribute pointer to whatever was passed in
	glVertexAttribPointer((GLuint)index, 4, GL_FLOAT, GL_FALSE, 0, 0);

	// set the attribute index
	glEnableVertexAttribArray(index);

	// store this in our map
	vertexBuffers.insert(std::make_pair(index, object));

	// we done here, unbind the mesh
#if defined(__APPLE__)
	glBindVertexArrayAPPLE((0));
#else
	glBindVertexArray(0);
#endif
}

void Cross::DynamicSubMesh::addBuffer(Color* data, UInt index)
{
	// @TODO this, remember about the 255 to 1.0f thingy
	// @TODO this store this in our map
}

void Cross::DynamicSubMesh::updateBuffer(Vector2* data, UInt index)
{
	// bind the mesh so that we can modify it
#if defined(__APPLE__)
	glBindVertexArrayAPPLE((vertexArray));
#else
	glBindVertexArray(vertexArray);
#endif

	// bind the vertex buffer
	glBindBuffer(GL_ARRAY_BUFFER, vertexBuffers[index]);

	// set the data of this buffer
	glBufferData(GL_ARRAY_BUFFER, (vertexCount * 2) * sizeof(GLfloat), (float*)data, GL_STATIC_DRAW);

	// we done here, unbind the mesh
#if defined(__APPLE__)
	glBindVertexArrayAPPLE((0));
#else
	glBindVertexArray(0);
#endif
}

void Cross::DynamicSubMesh::updateBuffer(Vector3* data, UInt index)
{
	// bind the mesh so that we can modify it
#if defined(__APPLE__)
	glBindVertexArrayAPPLE((vertexArray));
#else
	glBindVertexArray(vertexArray);
#endif

	// bind the vertex buffer
	glBindBuffer(GL_ARRAY_BUFFER, vertexBuffers[index]);

	// set the data of this buffer
	glBufferData(GL_ARRAY_BUFFER, (vertexCount * 3) * sizeof(GLfloat), (float*)data, GL_STATIC_DRAW);

	// we done here, unbind the mesh
#if defined(__APPLE__)
	glBindVertexArrayAPPLE((0));
#else
	glBindVertexArray(0);
#endif
}

void Cross::DynamicSubMesh::updateBuffer(Vector4* data, UInt index)
{
	// bind the mesh so that we can modify it
#if defined(__APPLE__)
	glBindVertexArrayAPPLE((vertexArray));
#else
	glBindVertexArray(vertexArray);
#endif

	// bind the vertex buffer
	glBindBuffer(GL_ARRAY_BUFFER, vertexBuffers[index]);

	// set the data of this buffer
	glBufferData(GL_ARRAY_BUFFER, (vertexCount * 4) * sizeof(GLfloat), (float*)data, GL_STATIC_DRAW);

	// we done here, unbind the mesh
#if defined(__APPLE__)
	glBindVertexArrayAPPLE((0));
#else
	glBindVertexArray(0);
#endif
}

void Cross::DynamicSubMesh::updateBuffer(Color* data, UInt index)
{
	// @TODO this, remember about the 255 to 1.0f thingy
}
