#include <Cross/Graphics/Font.h>
#include <Cross/Graphics/Color.h>
#include <Cross/Utility/Utility.h>

// @TODO remove this
#include <iostream>
using namespace std;

Cross::Font::Font(FT_Face face, UInt size)
{
	this->face = face;

	glGenTextures(1, &texture);
	glBindTexture(GL_TEXTURE_2D, texture);
	glTexParameteri(GL_TEXTURE_2D, GL_TEXTURE_WRAP_S, GL_REPEAT);
	glTexParameteri(GL_TEXTURE_2D, GL_TEXTURE_WRAP_T, GL_REPEAT);
	glTexEnvf(GL_TEXTURE_ENV, GL_TEXTURE_ENV_MODE, GL_MODULATE);
	glTexParameterf(GL_TEXTURE_2D, GL_TEXTURE_MIN_FILTER, GL_LINEAR);
	glTexParameterf(GL_TEXTURE_2D, GL_TEXTURE_MAG_FILTER, GL_LINEAR);
	glPixelStorei(GL_UNPACK_ALIGNMENT, 1);

	this->size = size;
}

Cross::Font* Cross::Font::loadFont(const std::string& font, UInt size)
{
	FT_Face face;
	FT_Error error = FT_New_Face(__freetypeLibrary, font.c_str(), 0, &face);
	if (error == FT_Err_Unknown_File_Format)
	{
		// @TODO error checking
		cout << "Font is an unknown format." << endl;
		return nullptr;
	}
	else if (error)
	{
		// @TODO error checking
		cout << "Failed to load font." << endl;
		return nullptr;
	}
	error = FT_Set_Char_Size(face, 50 * size, 0, 100, 0);
	if (error)
	{
		// @TODO error checking
		cout << "Failed to set character size." << endl;
		return nullptr;
	}
	return new Font(face, size);
}

void Cross::Font::draw(const std::string& text)
{
	// create the texture buffer
	Byte* data = new Byte[1024 * 1024 * 4];
	Color* buffer = (Color*)data;
	Utility::zeroMemory(buffer, 1024 * 1024 * sizeof(Color));

	// set the position of the font
	FT_Vector pen;
	pen.x = 0;
	pen.y = 0;

	// set our transform matrix (to the identity matrix)
	FT_Matrix matrix;
	matrix.xx = (FT_Fixed)(1 * 0x10000L);
	matrix.xy = (FT_Fixed)(0 * 0x10000L);
	matrix.yx = (FT_Fixed)(0 * 0x10000L);
	matrix.yy = (FT_Fixed)(1 * 0x10000L);

	// render each character
	for (UInt n = 0; n < text.length(); n++)
	{
		// transform the font character
		FT_Set_Transform(face, &matrix, &pen);

		// load glyph image into the slot
		FT_Error error = FT_Load_Char(face, text[n], FT_LOAD_RENDER);
		if (error)
		{
			continue; // @TODO ignore errors?
		}

		// now, draw to our surface
		FT_Int i, j, p, q;
		FT_Int x_max = face->glyph->bitmap_left + face->glyph->bitmap.width;
		FT_Int y_max = size - face->glyph->bitmap_top + face->glyph->bitmap.rows;
		for (i = face->glyph->bitmap_left, p = 0; i < x_max; i++, p++)
		{
			for (j = size - face->glyph->bitmap_top, q = 0; j < y_max; j++, q++)
			{
				if (i < 0 || j < 0 || i >= 1024 || j >= 1024)
					continue;

				buffer[(1024 - j) * 1024 + i].a |= face->glyph->bitmap.buffer[q * face->glyph->bitmap.width + p];
			}
		}

		// increment pen position
		pen.x += face->glyph->advance.x;
		pen.y += face->glyph->advance.y;
	}

	// update the texture
	glBindTexture(GL_TEXTURE_2D, texture);
	glTexImage2D(GL_TEXTURE_2D, 0, GL_RGBA, 1024, 1024, 0, GL_RGBA, GL_UNSIGNED_BYTE, buffer);

	// free the buffer
	delete[] data;
}

Cross::Texture Cross::Font::getTexture()
{
	return Texture(texture);
}
