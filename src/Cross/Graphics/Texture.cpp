#include <Cross/Graphics/Texture.h>
#include <Cross/Graphics/Color.h>
#include <Cross/Utility/File.h>
#include <Cross/Utility/Utility.h>

Cross::Texture::Texture()
{
}

Cross::Texture* Cross::Texture::loadBitmap(const std::string& fileName)
{
	Byte* fileData;
	UInt fileSize;
	if (!File::getData(fileName, &fileData, &fileSize))
	{
		// @TODO error checking: failed to open file
	}
	if (fileData[0] != 'B' || fileData[1] != 'M')
	{
		// @TODO error checking: invalid bitmap
	}
	UInt dataSize = Utility::build<UInt>(fileData + 2);
	UInt dataStart = Utility::build<UInt>(fileData + 10);
	UInt width = Utility::build<UInt>(fileData + 18);
	UInt height = Utility::build<UInt>(fileData + 22);
	short bits = Utility::build<UInt>(fileData + 28);
	Cross::Color* colors = new Cross::Color[width * height];
	UInt dataPos = dataStart;
	//for (int h = height - 1; h >= 0; h--)
	for (unsigned int h = 0; h < height; h++)
	{
		for (unsigned int w = 0; w < width; w++)
		{
			if (bits == 24)
			{
				colors[h * width + w].b = fileData[dataPos++];
				colors[h * width + w].g = fileData[dataPos++];
				colors[h * width + w].r = fileData[dataPos++];
			}
			if (bits == 32)
			{
				colors[h * width + w].b = fileData[dataPos++];
				colors[h * width + w].g = fileData[dataPos++];
				colors[h * width + w].r = fileData[dataPos++];
				colors[h * width + w].a = fileData[dataPos++];
			}
		}
	}
	
	Byte* data = new Byte[width * height * 4];
	//File::getData(fileName, &data);
	UInt pos = 0;
	for (UInt i = 0; i < width * height; i++)
	{
		data[pos++] = colors[i].r;
		data[pos++] = colors[i].g;
		data[pos++] = colors[i].b;
		data[pos++] = colors[i].a;
	}
	delete[] colors;
	
	Texture* ret = new Texture();
	glGenTextures(1, &ret->texture);
	glBindTexture(GL_TEXTURE_2D, ret->texture);
	glTexParameteri(GL_TEXTURE_2D, GL_TEXTURE_WRAP_S, GL_REPEAT);
	glTexParameteri(GL_TEXTURE_2D, GL_TEXTURE_WRAP_T, GL_REPEAT);
	glTexEnvf(GL_TEXTURE_ENV, GL_TEXTURE_ENV_MODE, GL_MODULATE);
	glTexParameterf(GL_TEXTURE_2D, GL_TEXTURE_MIN_FILTER, GL_LINEAR);
	glTexParameterf(GL_TEXTURE_2D, GL_TEXTURE_MAG_FILTER, GL_LINEAR);
	glPixelStorei(GL_UNPACK_ALIGNMENT, 1);
	glTexImage2D(GL_TEXTURE_2D, 0, GL_RGBA, width, height, 0, GL_RGBA, GL_UNSIGNED_BYTE, data);
	delete[] data;
	return ret;
}

Cross::Texture::Texture(UInt texture)
{
	this->texture = texture;
}

Cross::Texture::~Texture()
{
}

UInt Cross::Texture::getTexture() const
{
	return texture;
}
