#include <Cross/Graphics/Shader.h>
#include <Cross/Graphics/Texture.h>
#include <Cross/Utility/File.h>
#include <sstream>

// @TODO remove this:
#include <iostream>
using namespace std;

Cross::Shader::Shader(std::string vertex, std::string fragment)
{
	vertexID = 0;
	fragmentID = 0;
	shaderID = 0;

	vertexID = glCreateShader(GL_VERTEX_SHADER);
	fragmentID = glCreateShader(GL_FRAGMENT_SHADER);

	std::string vertexFile, fragmentFile;
	if (!File::getText(vertex, vertexFile))
	{
		glDeleteShader(vertexID);
		glDeleteShader(fragmentID);
		cout << "Failed to load vertex shader." << endl;
		return;
	}
	if (!File::getText(fragment, fragmentFile))
	{
		glDeleteShader(vertexID);
		glDeleteShader(fragmentID);
		cout << "Failed to load fragment shader." << endl;
		return;
	}

	const char* vertexString = vertexFile.c_str();
	const char* fragmentString = fragmentFile.c_str();

	// variables for compile-error checking
	Int testShader;
	char buffer[1024];
    GLsizei length = 0;

	glShaderSource(vertexID, 1, &vertexString, nullptr);
	glCompileShader(vertexID);
    
	glGetShaderiv(vertexID, GL_COMPILE_STATUS, &testShader);
	if (!testShader)
	{
		glGetShaderInfoLog(vertexID, 1024, &length, buffer);
		std::stringstream ss(std::stringstream::out | std::stringstream::in);
		ss << "Error compiling " << vertex << ":\n" << buffer;
		glDeleteShader(vertexID);
		glDeleteShader(fragmentID);
		cout << ss.str() << endl;
		return;
	}

	glShaderSource(fragmentID, 1, &fragmentString, nullptr);
	glCompileShader(fragmentID);
    
	glGetShaderiv(fragmentID, GL_COMPILE_STATUS, &testShader);
	if (!testShader)
	{
		glGetShaderInfoLog(fragmentID, 1024, &length, buffer);
		std::stringstream ss(std::stringstream::out | std::stringstream::in);
		ss << "Error compiling " << fragment << ":\n" << buffer;
		glDeleteShader(vertexID);
		glDeleteShader(fragmentID);
		cout << ss.str() << endl;
		return;
	}

	shaderID = glCreateProgram();
	glAttachShader(shaderID, vertexID);
	glAttachShader(shaderID, fragmentID);

	glLinkProgram(shaderID);

	glDeleteShader(vertexID);
	glDeleteShader(fragmentID);
    
	glGetProgramiv(shaderID, GL_LINK_STATUS, &testShader);
	if (!testShader)
	{
		glGetProgramInfoLog(shaderID, 1024, &length, buffer);
		std::stringstream ss(std::stringstream::out | std::stringstream::in);
		ss << buffer;
		glDeleteProgram(shaderID);
		glDeleteShader(vertexID);
		glDeleteShader(fragmentID);
		// @TODO error checking
		cout << ss.str() << endl;
		return;
	}

	glValidateProgram(shaderID);

	glGetProgramiv(shaderID, GL_VALIDATE_STATUS, &testShader);
	if (!testShader)
	{
		glGetProgramInfoLog(shaderID, 1024, &length, buffer);
		std::stringstream ss(std::stringstream::out | std::stringstream::in);
		ss << buffer;
		glDeleteProgram(shaderID);
		glDeleteShader(vertexID);
		glDeleteShader(fragmentID);
		// @TODO error checking
		cout << ss.str() << endl;
		return;
	}
}

Cross::Shader::~Shader()
{
	if (shaderID)
	{
		glDeleteProgram(shaderID);
	}
	if (vertexID)
	{
		glDeleteShader(vertexID);
	}
	if (fragmentID)
	{
		glDeleteShader(fragmentID);
	}
}

void Cross::Shader::begin()
{
	textureID = 0;
	textures.clear();
	glUseProgram(shaderID);
}

void Cross::Shader::end()
{
	glUseProgram(0);
}

void Cross::Shader::setMatrix(const std::string& name, const Matrix& matrix)
{
	glUniformMatrix4fv(glGetUniformLocation(shaderID, name.c_str()), 1, GL_FALSE, matrix.getData());
}

void Cross::Shader::setTexture(const std::string& name, Texture* texture)
{
	int id;
	std::map<std::string, int>::iterator it = textures.find(name);
	if (it != textures.end())
	{
		id = it->second;
	}
	else
	{
		id = textureID++;
		textures.insert(std::make_pair(name, id));
	}
	int myTexture = glGetUniformLocation(shaderID, name.c_str());
	glUniform1i(myTexture, id);
	glActiveTexture(GL_TEXTURE0 + id);
	glBindTexture(GL_TEXTURE_2D, texture->getTexture());
	textureID++;
}

void Cross::Shader::setTexture(const std::string& name, Texture texture)
{
	int id;
	std::map<std::string, int>::iterator it = textures.find(name);
	if (it != textures.end())
	{
		id = it->second;
	}
	else
	{
		id = textureID++;
		textures.insert(std::make_pair(name, id));
	}
	int myTexture = glGetUniformLocation(shaderID, name.c_str());
	glUniform1i(myTexture, id);
	glActiveTexture(GL_TEXTURE0 + id);
	glBindTexture(GL_TEXTURE_2D, texture.getTexture());
	textureID++;
}

//void Cross::Shader::setTexture(const std::string& name, UInt texture)
//{
//	int id;
//	std::map<std::string, int>::iterator it = textures.find(name);
//	if (it != textures.end())
//	{
//		id = it->second;
//	}
//	else
//	{
//		id = textureID++;
//		textures.insert(std::make_pair(name, id));
//	}
//	int myTexture = glGetUniformLocation(shaderID, name.c_str());
//	glUniform1i(myTexture, id);
//	glActiveTexture(GL_TEXTURE0 + id);
//	glBindTexture(GL_TEXTURE_2D, texture);
//	textureID++;
//}

void Cross::Shader::setArray(const std::string& name, Vector4* vectors, UInt size)
{
	glUniform4fv(glGetUniformLocation(shaderID, name.c_str()), size, (Float*)vectors);
}

void Cross::Shader::bindAttribute(UInt index, const std::string& name)
{
	glBindAttribLocation(shaderID, index, name.c_str());
	glLinkProgram(shaderID); // @TODO make shaders not call this every time
}

int Cross::Shader::getID()
{
	return shaderID;
}
