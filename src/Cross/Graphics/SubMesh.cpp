#include <Cross/Graphics/SubMesh.h>

Cross::SubMesh::SubMesh(UInt vertexCount, Type type)
{
#if defined(__APPLE__)
	glGenVertexArraysAPPLE(1, &vertexArray);
#else
	glGenVertexArrays(1, &vertexArray);
#endif
	this->vertexCount = vertexCount;
	this->type = type;
}

void Cross::SubMesh::addBuffer(Vector2* data, UInt index)
{
	// bind the mesh so that we can modify it
#if defined(__APPLE__)
	glBindVertexArrayAPPLE((vertexArray));
#else
	glBindVertexArray(vertexArray);
#endif

	// generate a new vertex buffer
	UInt object;
	glGenBuffers(1, &object);

	// bind the vertex buffer
	glBindBuffer(GL_ARRAY_BUFFER, object);

	// set the data of this buffer
	glBufferData(GL_ARRAY_BUFFER, (vertexCount * 2) * sizeof(GLfloat), (float*)data, GL_STATIC_DRAW);

	// setup the attribute pointer to whatever was passed in
	glVertexAttribPointer((GLuint)index, 2, GL_FLOAT, GL_FALSE, 0, 0);

	// set the attribute index
	glEnableVertexAttribArray(index);

	// we done here, unbind the mesh
#if defined(__APPLE__)
	glBindVertexArrayAPPLE((0));
#else
	glBindVertexArray(0);
#endif
}

void Cross::SubMesh::addBuffer(Vector3* data, UInt index)
{
	// bind the mesh so that we can modify it
#if defined(__APPLE__)
	glBindVertexArrayAPPLE((vertexArray));
#else
	glBindVertexArray(vertexArray);
#endif

	// generate a new vertex buffer
	UInt object;
	glGenBuffers(1, &object);

	// bind the vertex buffer
	glBindBuffer(GL_ARRAY_BUFFER, object);

	// set the data of this buffer
	glBufferData(GL_ARRAY_BUFFER, (vertexCount * 3) * sizeof(GLfloat), (float*)data, GL_STATIC_DRAW);

	// setup the attribute pointer to whatever was passed in
	glVertexAttribPointer(index, 3, GL_FLOAT, GL_FALSE, 0, 0);

	glVertexAttrib1s(0, 0);

	// set the attribute index
	glEnableVertexAttribArray(0);

	// we done here, unbind the mesh
#if defined(__APPLE__)
	glBindVertexArrayAPPLE((0));
#else
	glBindVertexArray(0);
#endif
}

void Cross::SubMesh::addBuffer(Vector4* data, UInt index)
{
	// bind the mesh so that we can modify it
#if defined(__APPLE__)
	glBindVertexArrayAPPLE((vertexArray));
#else
	glBindVertexArray(vertexArray);
#endif

	// generate a new vertex buffer
	UInt object;
	glGenBuffers(1, &object);

	// bind the vertex buffer
	glBindBuffer(GL_ARRAY_BUFFER, object);

	// set the data of this buffer
	glBufferData(GL_ARRAY_BUFFER, (vertexCount * 4) * sizeof(GLfloat), (float*)data, GL_STATIC_DRAW);

	// setup the attribute pointer to whatever was passed in
	glVertexAttribPointer((GLuint)index, 4, GL_FLOAT, GL_FALSE, 0, 0);

	// set the attribute index
	glEnableVertexAttribArray(index);

	// we done here, unbind the mesh
#if defined(__APPLE__)
	glBindVertexArrayAPPLE((0));
#else
	glBindVertexArray(0);
#endif
}

void Cross::SubMesh::addBuffer(Color* data, UInt index)
{
	// @TODO this, remember about the 255 to 1.0f thingy
}

void Cross::SubMesh::updateBuffer(Vector2* data, UInt index)
{
	// @TODO exception
}

void Cross::SubMesh::updateBuffer(Vector3* data, UInt index)
{
	// @TODO exception
}

void Cross::SubMesh::updateBuffer(Vector4* data, UInt index)
{
	// @TODO exception
}

void Cross::SubMesh::updateBuffer(Color* data, UInt index)
{
	// @TODO exception
}

void Cross::SubMesh::setType(Type type)
{
	this->type = type;
}

Cross::SubMesh::Type Cross::SubMesh::getType() const
{
	return type;
}

void Cross::SubMesh::draw()
{
	// draw this sh!te
#if defined(__APPLE__)
	glBindVertexArrayAPPLE(vertexArray);
#else
	glBindVertexArray(vertexArray);
#endif
	glDrawArrays(type, 0, vertexCount);
#if defined(__APPLE__)
	glBindVertexArrayAPPLE(0);
#else
	glBindVertexArray(0);
#endif
}
