#include <Cross/Graphics/Graphics.h>
#include <Cross/Window.h>

Cross::Graphics::Graphics(Window* window)
{
#if defined(_WIN32)
	// setup the pixel format descriptor
	pfd.nSize = sizeof(PIXELFORMATDESCRIPTOR);
	pfd.nVersion = 0;
	pfd.dwFlags = PFD_DOUBLEBUFFER | PFD_SUPPORT_OPENGL | PFD_DRAW_TO_WINDOW;
	pfd.iPixelType = PFD_TYPE_RGBA;
	pfd.cColorBits = 32;
	pfd.cRedBits = 0;
	pfd.cRedShift = 0;
	pfd.cGreenBits = 0;
	pfd.cGreenShift = 0;
	pfd.cBlueBits = 0;
	pfd.cBlueShift = 0;
	pfd.cAlphaBits = 0;
	pfd.cAlphaShift = 0;
	pfd.cAccumBits = 0;
	pfd.cAccumRedBits = 0;
	pfd.cAccumGreenBits = 0;
	pfd.cAccumBlueBits = 0;
	pfd.cAccumAlphaBits = 0;
	pfd.cDepthBits = 32;
	pfd.cStencilBits = 0;
	pfd.cAuxBuffers = 0;
	pfd.iLayerType = PFD_MAIN_PLANE;
	pfd.bReserved = 0;
	pfd.dwLayerMask = 0;
	pfd.dwVisibleMask = 0;
	pfd.dwDamageMask = 0;

	// get the device handle for the window
	deviceContext = GetDC(window->getHandle());
	if (!deviceContext)
	{
		// @TODO error handling
	}

	// set the pixel format
	pixelFormat = ChoosePixelFormat(deviceContext, &pfd);
	if (!SetPixelFormat(deviceContext, pixelFormat, &pfd))
	{
		// @TODO error handling
	}

	// create opengl render context and make it current
	renderContext = wglCreateContext(deviceContext);
	if (!renderContext)
	{
		// @TODO error handling
	}
	if (!wglMakeCurrent(deviceContext, renderContext))
	{
		// @TODO error handling
	}
#elif defined(unix)
	// also create a new GL context for rendering
	GLint attribs[] = {
		GLX_CONTEXT_MAJOR_VERSION_ARB, 3,
		GLX_CONTEXT_MINOR_VERSION_ARB, 0,
	0 };
	context = glXCreateContextAttribsARB(window->display, window->config[0], 0, true, attribs);
	glXMakeCurrent(window->display, window->handle, context);
#endif

	GLenum error = glewInit();
	if (error != GLEW_OK)
	{
		// @TODO error handling
	}

	// setup the states
	glEnable(GL_CULL_FACE);
	glCullFace(GL_BACK);
	glEnable(GL_DEPTH_TEST);
}

void Cross::Graphics::clear(Cross::Color color)
{
	glClearColor(color.r / 255.0f, color.g / 255.0f, color.b / 255.0f, color.a / 255.0f);
	glClear(GL_COLOR_BUFFER_BIT | GL_DEPTH_BUFFER_BIT);
}
