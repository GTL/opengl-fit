#include <Cross/Graphics/RenderTarget.h>

// @TODO remove this
#include <iostream>
using namespace std;

Cross::RenderTarget::Viewport Cross::RenderTarget::viewport;

Cross::RenderTarget::RenderTarget(UInt width, UInt height, UInt textures)
{
	this->width = width;
	this->height = height;
	this->textureCount = textures;
	
	this->textures = new UInt[textures];

	glGenFramebuffers(1, &frameBuffer);
	glBindFramebuffer(GL_FRAMEBUFFER, frameBuffer);

	glGenTextures(textures, this->textures);

	for (UInt i = 0; i < textures; i++)
	{
		glBindTexture(GL_TEXTURE_2D, this->textures[i]);
		glTexImage2D(GL_TEXTURE_2D, 0, GL_RGB, width, height, 0, GL_RGB, GL_UNSIGNED_BYTE, 0);
		glTexParameteri(GL_TEXTURE_2D, GL_TEXTURE_MAG_FILTER, GL_NEAREST);
		glTexParameteri(GL_TEXTURE_2D, GL_TEXTURE_MIN_FILTER, GL_NEAREST);
		glFramebufferTexture2D(GL_FRAMEBUFFER, GL_COLOR_ATTACHMENT0 + i, GL_TEXTURE_2D, this->textures[i], 0);
	}
	
	/*glGenRenderbuffers(1, &renderBuffer);
	glBindRenderbuffer(GL_RENDERBUFFER_EXT, renderBuffer);
	glRenderbufferStorage(GL_RENDERBUFFER_EXT, GL_DEPTH_COMPONENT24, width, height);
	glFramebufferRenderbuffer(GL_FRAMEBUFFER, GL_DEPTH_ATTACHMENT, GL_RENDERBUFFER, renderBuffer);*/
	
	glBindFramebuffer(GL_FRAMEBUFFER, 0);
	//glBindRenderbuffer(GL_RENDERBUFFER, 0);
}

Cross::RenderTarget::~RenderTarget()
{
	delete[] textures;
}

void Cross::RenderTarget::begin()
{
	glGetIntegerv(GL_VIEWPORT, viewport.vp);
	glViewport(0, 0, width, height);
	glBindFramebuffer(GL_FRAMEBUFFER, frameBuffer);
	//glBindRenderbuffer(GL_RENDERBUFFER, renderBuffer);

	if (textureCount > 1)
	{
		GLenum* buffers = new GLenum[textureCount];
		for (UInt i = 0; i < textureCount; i++)
		{
			buffers[i] = GL_COLOR_ATTACHMENT0 + i;
		}
		glDrawBuffers(textureCount, buffers);
		delete[] buffers;
	}
}

void Cross::RenderTarget::end()
{	
	glBindFramebuffer(GL_FRAMEBUFFER, 0);
	//glBindRenderbuffer(GL_RENDERBUFFER, 0);
	
	glViewport(viewport.x, viewport.y, viewport.width, viewport.height);
}

Cross::Texture Cross::RenderTarget::getTexture(UInt texture)
{
	return Cross::Texture(textures[texture]);
}
