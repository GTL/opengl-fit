#include <Cross/Graphics/Mesh.h>
#include <Cross/Graphics/DynamicSubMesh.h>
#include <Cross/Utility/File.h>
#include <Cross/Math/Vector2.h>
#include <Cross/Math/Vector3.h>
#include <Cross/Math/Vector4.h>
#include <Cross/Utility/Utility.h>

// @TODO remove this... eventually
#include <iostream>
using namespace std;

namespace
{
	struct Index
	{
		std::vector<int> vertices, textureCoords, normals;
	};
}

Cross::SubMesh* Cross::Mesh::addSubMesh(UInt vertexCount, SubMesh::Type type)
{
	return addStaticSubMesh(vertexCount, type);
}

Cross::SubMesh* Cross::Mesh::addStaticSubMesh(UInt vertexCount, SubMesh::Type type)
{
	SubMesh* ret = new SubMesh(vertexCount, type);
	subMeshes.push_back(ret);
	return ret;
}

Cross::SubMesh* Cross::Mesh::addDynamicSubMesh(UInt vertexCount, SubMesh::Type type)
{
	SubMesh* ret = new DynamicSubMesh(vertexCount, type);
	subMeshes.push_back(ret);
	return ret;
}

Cross::SubMesh* Cross::Mesh::getSubMesh(unsigned int index) const
{
	return subMeshes[index];
}

UInt Cross::Mesh::getSubMeshCount()
{
	return subMeshes.size();
}

void Cross::Mesh::draw()
{
	for (std::vector<SubMesh*>::iterator i = subMeshes.begin(); i != subMeshes.end(); i++)
	{
		(*i)->draw();
	}
}

Cross::Mesh* Cross::Mesh::loadObj(const std::string& fileName)
{
	// @TODO get rid of this shit, it is terrible

	std::vector<std::pair<Vector3, int> > vertices;
	std::vector<std::pair<Vector3, int> > textureCoords;
	std::vector<std::pair<Vector3, int> > normals;
	std::map<std::string, Index> indices;

	std::string result;
	Cross::File::getText(fileName, result);

	std::string groupName = "default";

	for (UInt i = 0; i < result.size();)
	{
		char cur = result[i++];
		if (cur == ' ' || cur == 13 || cur == 10) // skip excessive spacing
		{
			continue;
		}
		else
		{
			bool skipLine = false;
			switch (tolower(cur))
			{
			case '#': // comments
				{
					skipLine = true;
				}
				break;
			case 'v':
				{
					std::vector<std::pair<Vector3, int> >* pull;
					if (cur == 'v' && result[i] == ' ')
					{
						while (result[++i] == ' ');
						pull = &vertices;
					}
					else if (result[i] == 't' && result[i + 1] == ' ')
					{
						while (result[++i] == ' ');
						pull = &textureCoords;
					}
					else if (result[i] == 'n' && result[i + 1] == ' ')
					{
						while (result[++i] == ' ');
						pull = &normals;
					}
					else
					{
						cout << "Invalid obj file." << endl;
						return nullptr;
					}
					std::vector<std::string> data;
					do
					{
						std::string number;
						while (isdigit(result[i]) || result[i] == '-' || result[i] == '.')
						{
							number += result[i];
							i++;
						}
						data.push_back(number);
						if (result[i] != ' ' && result[i] != 13 && result[i] != 10)
						{
							char b = result[i];
							skipLine = true;
							break;
						}
						if (result[i] == ' ')
						{
							while (result[++i] == ' ');
						}
					}
					while (result[i] != 13 && result[i] != 10);
					if (!skipLine)
					{
						skipLine = (data.size() == 0);
					}
					if (!skipLine)
					{
						if (data.size() < 2)
						{
							cout << "Cannot load mesh with less than 2D components." << endl;
							return nullptr;
						}
						else if (data.size() > 3)
						{
							cout << "Cannot load mesh with more than 3D components." << endl;
							return nullptr;
						}
						else
						{
							Vector3 insert;
							for (unsigned int j = 0; j < data.size(); j++)
							{
								insert[j] = Utility::stringTo<float>(data[j]);
							}
							pull->push_back(std::make_pair(insert, data.size()));
						}
					}
				}
				break;
			case 'f':
				{
					Index* pull;
					if (cur == 'f' && result[i] == ' ')
					{
						while (result[++i] == ' ');
						std::map<std::string, Index>::iterator i = indices.find(groupName);
						if (i == indices.end())
						{
							indices.insert(std::make_pair(groupName, Index()));
							i = indices.find(groupName);
						}
						pull = &i->second;
					}
					else
					{
						cout << "Invalid obj file." << endl;
						return nullptr;
					}
					std::vector<std::string> data;
					do
					{
						std::string number;
						while (isdigit(result[i]) || result[i] == '-' || result[i] == '.' || result[i] == '/')
						{
							number += result[i];
							i++;
						}
						data.push_back(number);
						if (result[i] != ' ' && result[i] != 13 && result[i] != 10)
						{
							char b = result[i];
							skipLine = true;
							break;
						}
						if (result[i] == ' ')
						{
							while (result[++i] == ' ');
						}
					}
					while (result[i] != 13 && result[i] != 10);
					if (!skipLine)
					{
						skipLine = (data.size() == 0);
					}
					if (!skipLine)
					{
						if (data.size() < 2)
						{
							cout << "Cannot load mesh with less than 2D components." << endl;
							return 0;
						}
						else if (data.size() > 3)
						{
							cout << "Cannot load mesh with more than 3D components." << endl;
							return 0;
						}
						else
						{
							for (unsigned int j = 0; j < data.size(); j++)
							{
								if (data[j][0] == '/' || data[j][data[j].length() - 1] == '/')
								{
									cout << "Invalid obj file." << endl;
								}
								if (data[j].find('/') == std::string::npos)
								{
									pull->vertices.push_back(Utility::stringTo<int>(data[j]));
								}
								else
								{
									data[j] += "/";
									int count = 0;
									while (data[j].find('/') != std::string::npos)
									{
										count++;
										std::string element = data[j].substr(0, data[j].find('/'));
										if (element == "")
										{
											data[j] = data[j].substr(data[j].find('/') + 1);
											continue;
										}
										switch (count)
										{
										case 1:
											pull->vertices.push_back(Utility::stringTo<int>(element));
											break;
										case 2:
											pull->textureCoords.push_back(Utility::stringTo<int>(element));
											break;
										case 3:
											pull->normals.push_back(Utility::stringTo<int>(element));
											break;
										case 4:
											cout << "Invalid obj file." << endl;
											return nullptr;
										}
										data[j] = data[j].substr(data[j].find('/') + 1);
									}
								}
							}
						}
					}
				}
				break;
			case 'g':
				{
					if (result[i++] != ' ')
					{
						cout << "Invalid obj file." << endl;
						return nullptr;
					}
					groupName = "";
					while (result[i] != 13 && result[i] != 10)
					{
						groupName += result[i++];
					}
				}
				break;
			/*default:
				Ex("Invalid obj file.");
				break;*/
			}
			if (skipLine)
			{
				do
				{
					cur = result[i++];
				} while (cur != 13 && cur != 10);
			}
		}
	}

	Mesh* newMesh = new Mesh();

	for (std::map<std::string, Index>::iterator i = indices.begin(); i != indices.end(); i++)
	{
		if (i->second.vertices.size() == 0)
		{
			cout << "Invalid obj file." << endl;
			return nullptr;
		}
		if (i->second.textureCoords.size() != 0 && i->second.vertices.size() != i->second.textureCoords.size())
		{
			cout << "Invalid obj file." << endl;
			return nullptr;
		}
		if (i->second.normals.size() != 0 && i->second.vertices.size() != i->second.normals.size())
		{
			cout << "Invalid obj file." << endl;
			return nullptr;
		}
		SubMesh* newSubMesh = newMesh->addSubMesh(i->second.vertices.size(), SubMesh::TRIANGLES);
		int bufferCount = 0;
		if (vertices[i->second.vertices[0]].second == 2)
		{
			Vector2* positions = new Vector2[i->second.vertices.size()];
			for (UInt j = 0; j < i->second.vertices.size(); j++)
			{
				positions[j] = vertices[i->second.vertices[j] - 1].first;
			}
			newSubMesh->addBuffer(positions, bufferCount++);
			delete[] positions;
		}
		else
		{
			Vector3* positions = new Vector3[i->second.vertices.size()];
			for (UInt j = 0; j < i->second.vertices.size(); j++)
			{
				positions[j] = vertices[i->second.vertices[j] - 1].first;
			}
			newSubMesh->addBuffer(positions, bufferCount++);
			delete[] positions;
		}
		if (i->second.textureCoords.size() != 0)
		{
			if (textureCoords[i->second.textureCoords[0]].second == 2)
			{
				Vector2* texs = new Vector2[i->second.textureCoords.size()];
				for (UInt j = 0; j < i->second.textureCoords.size(); j++)
				{
					texs[j] = textureCoords[i->second.textureCoords[j] - 1].first;
				}
				newSubMesh->addBuffer(texs, bufferCount++);
				delete[] texs;
			}
			else
			{
				Vector3* texs = new Vector3[i->second.textureCoords.size()];
				for (UInt j = 0; j < i->second.textureCoords.size(); j++)
				{
					texs[j] = textureCoords[i->second.textureCoords[j] - 1].first;
				}
				newSubMesh->addBuffer(texs, bufferCount++);
				delete[] texs;
			}
		}
		if (i->second.normals.size() != 0)
		{
			if (normals[i->second.normals[0]].second == 2)
			{
				Vector2* norms = new Vector2[i->second.normals.size()];
				for (UInt j = 0; j < i->second.normals.size(); j++)
				{
					norms[j] = normals[i->second.normals[j] - 1].first;
				}
				//newSubMesh->addNormals(norms);
				delete[] norms;
			}
			else
			{
				Vector3* norms = new Vector3[i->second.normals.size()];
				for (UInt j = 0; j < i->second.normals.size(); j++)
				{
					norms[j] = vertices[i->second.normals[j] - 1].first;
				}
				//newSubMesh->addNormals(norms);
				delete[] norms;
			}
		}
	}

	return newMesh;
}