#include <Cross/Application.h>
#include <Cross/Window.h>
#include <Cross/Graphics/Graphics.h>
#include <Cross/Input/Input.h>
#include <Cross/Utility/Time.h>

// @TODO remove this
#include <iostream>
using namespace std;

// @TODO make sure this is okay as a global variable
FT_Library __freetypeLibrary;

Cross::Application* Cross::Application::application = nullptr;

bool Cross::Application::running = true;
#if defined(_WIN32)
wchar_t* Cross::Application::className = L"CrossWindow";
#endif

Cross::Application* Cross::Application::getApplication()
{
	return application;
}

void Cross::Application::quit()
{
	running = false;
}

void Cross::Application::sleep(UInt milliseconds)
{
	#if defined(_WIN32)
	Sleep(milliseconds);
	#endif
}

Cross::Application::Application()
{
	application = this;
}

void Cross::Application::initialize()
{
#if defined(_WIN32)
	// create the window class
	WNDCLASS wc;
	wc.style = CS_HREDRAW | CS_VREDRAW | CS_OWNDC;
	wc.lpfnWndProc = Cross::Window::windowProcedure;
	wc.cbClsExtra = 0;
	wc.cbWndExtra = 0;
	wc.hInstance = GetModuleHandle(nullptr);
	wc.hIcon = LoadIcon(0, IDI_APPLICATION);
	wc.hCursor = LoadCursor(0, IDC_ARROW);
	wc.hbrBackground = CreateSolidBrush(RGB(0, 0, 0));
	wc.lpszMenuName = 0;
	wc.lpszClassName = L"CrossWindow";

	// attempt to register the window class
	if (!RegisterClass(&wc))
	{
		// @TODO error handling
	}

	// select a window style for the window
	DWORD windowStyle = WS_OVERLAPPEDWINDOW;
	//DWORD windowStyle = WS_OVERLAPPED | WS_CAPTION | WS_SYSMENU | WS_MINIMIZEBOX;

	// calculate the proper window size
	RECT R = {0, 0, 800, 600};
	AdjustWindowRect(&R, windowStyle, false);

	// create the window
	HWND windowHandle;
	if (!(windowHandle = CreateWindow(className, L"Cross Framework", windowStyle, 50, 50, R.right - R.left, R.bottom - R.top, nullptr, nullptr, GetModuleHandle(nullptr), nullptr)))
	{
		// @TODO error handling
	}

	// update and show the window
	UpdateWindow(windowHandle);
	ShowWindow(windowHandle, SW_SHOW);

	// create the window object
	window = new Window(windowHandle, windowStyle);
#elif defined(unix)
	// load some glew stuff early so we can use it in creating the context
	//glGenVertexArraysAPPLE = (void(*)(GLsizei, const GLuint*))glXGetProcAddressARB((GLubyte*)"glGenVertexArrays");
	//glBindVertexArrayAPPLE = (void(*)(const GLuint))glXGetProcAddressARB((GLubyte*)"glBindVertexArray");
	//glDeleteVertexArraysAPPLE = (void(*)(GLsizei, const GLuint*))glXGetProcAddressARB((GLubyte*)"glGenVertexArrays");
	glXCreateContextAttribsARB = (GLXContext(*)(Display* dpy, GLXFBConfig config, GLXContext share_context, Bool direct, const int *attrib_list))glXGetProcAddressARB((GLubyte*)"glXCreateContextAttribsARB");
	glXChooseFBConfig = (GLXFBConfig*(*)(Display *dpy, int screen, const int *attrib_list, int *nelements))glXGetProcAddressARB((GLubyte*)"glXChooseFBConfig");
	glXGetVisualFromFBConfig = (XVisualInfo*(*)(Display *dpy, GLXFBConfig config))glXGetProcAddressARB((GLubyte*)"glXGetVisualFromFBConfig");

	Display* display = XOpenDisplay(nullptr);
	if (display == nullptr)
	{
		// @TODO error handling
		cout << "error" << endl;
	}

	XSetWindowAttributes windowAttributes;
	Int winmask;
	Int nMajorVer = 0;
	Int nMinorVer = 0;
	GLXFBConfig* config;
	int numConfigs = 0;
	static int fbAttribs[] = {
		GLX_RENDER_TYPE,   GLX_RGBA_BIT,
		GLX_X_RENDERABLE,  True,
		GLX_DRAWABLE_TYPE, GLX_WINDOW_BIT,
		GLX_DOUBLEBUFFER,  True,
		GLX_RED_SIZE, 8,
		GLX_BLUE_SIZE, 8,
		GLX_GREEN_SIZE, 8,
	0 };

	// Get Version info
	glXQueryVersion(display, &nMajorVer, &nMinorVer);
	if(nMajorVer == 1 && nMinorVer < 2)
	{
		// @TODO error handling
		cout << "error" << endl;
	}

	// get a new fb config that meets our attrib requirements
	config = glXChooseFBConfig(display, DefaultScreen(display), fbAttribs, &numConfigs);
	XVisualInfo* visual = glXGetVisualFromFBConfig(display, config[0]);

	// now create an X window
	windowAttributes.event_mask = ExposureMask | VisibilityChangeMask |
	KeyPressMask | PointerMotionMask | StructureNotifyMask | ButtonPressMask | ButtonReleaseMask |
	KeyReleaseMask;

	windowAttributes.border_pixel = 0;
	windowAttributes.bit_gravity = StaticGravity;
	windowAttributes.colormap = XCreateColormap(display,
	RootWindow(display, visual->screen),
	visual->visual, AllocNone);
	winmask = CWBorderPixel | CWBitGravity | CWEventMask| CWColormap;

	::Window handle = XCreateWindow(display, DefaultRootWindow(display), 20, 20, 800, 600, 0,
	visual->depth, InputOutput,
	visual->visual, winmask, &windowAttributes);

	Atom wmDelete = XInternAtom(display, "WM_DELETE_WINDOW", True);
	XSetWMProtocols(display, handle, &wmDelete, 1);

	XMapWindow(display, handle);

	XSizeHints hints;
	hints.flags = PMinSize | PMaxSize;
	hints.min_width = 800; hints.min_height = 600;
	hints.max_width = 800; hints.max_height = 600;
	XSetWMNormalHints(display, handle, &hints);

	// create the window object
	window = new Window(handle, display, visual, config);
#endif

	// create the graphics object
	graphics = new Cross::Graphics(window);
	
	// create the input object
	input = new Cross::Input(window);

	// initialize the freetype library
	FT_Error freetypeError = FT_Init_FreeType(&__freetypeLibrary);
	if (freetypeError)
	{
		// @TODO error checking
		cout << "Failed to initialize freetype." << endl;
	}

	// set the first time stamp...
	__previousTime = Time::getCounter();
}

void Cross::Application::update(float dt)
{
	input->update();
	window->update();
}

void Cross::Application::draw()
{
}

void Cross::Application::uninitialize()
{
}

#if defined(_WIN32) | defined(unix)
void Cross::Application::run()
{
	initialize();
	while (running)
	{
		sleep(1);
		float time = Time::getCounter();
		update(time - __previousTime);
		__previousTime = time;
		draw();
		#if defined(_WIN32)
		SwapBuffers(graphics->deviceContext);
		#elif defined(unix)
		glXSwapBuffers(window->display, window->handle);
		#endif
	}
	uninitialize();
}
#endif
