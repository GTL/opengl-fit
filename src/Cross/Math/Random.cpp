#include <Cross/Math/Random.h>
#include <time.h>
#include <math.h>

#define Seed() (seed = (multiplier * seed + addition) % modulus)

float Cross::Random::uniformDist(float a, float b, float u)
{
	if (u < 0)
	{
		return a;
	}
	if (u > 1)
	{
		return b;
	}
	return u * (b - a) + a;
}

float Cross::Random::exponentialDist(float beta, float u)
{
	if (u < 0.0)
	{
		u = 0.0;
	}
	if (u >= 0.999f)
	{
		u = 0.999f;
	}
	//return  -beta * log(u);
	return beta * (float)(log(-1.0 / (u - 1.0)));
}

float Cross::Random::weibullDist(float a, float b, float c, float u)
{
	if (u < 0.0)
	{
		u = 0.0;
	}
	if (u >= 0.999f)
	{
		u = 0.999f;
	}
	return b * (float)pow(log(-1.0 / (u - 1.0)), 1.0 / c) + a;
}

float Cross::Random::triangularDist(float xmin, float xmax, float c, float u)
{
	if (u < 0)
	{
		u = 0;
	}
	if (u > 1)
	{
		u = 1;
	}
	if (u < (c - xmin) / (xmax - xmin))
	{
		return sqrt(u * (xmin - xmax) * (xmin - c)) + xmin;
	}
	else
	{
		return xmax - sqrt((u - 1) * (xmin - xmax) * (xmax - c));
	}
}

float Cross::Random::normal(float mean, float variance, float x)
{
	return (1.0f / (float)sqrt(2.0f * 3.14159f * variance)) * exp(-(pow(x - mean, 2) / (2.0f * variance)));
}

Cross::Random::Random()
{
	seed = (unsigned int)time(0);
	multiplier = 16807;
	addition = 0;
	modulus = 2147483647;
}

Cross::Random::Random(const unsigned int seed)
{
	this->seed = seed;
	multiplier = 16807;
	addition = 0;
	modulus = 2147483647;
}

int Cross::Random::getInteger(int min, int max)
{
	int difference = max - min;
	return min + (Seed() % (difference + 1));
}

float Cross::Random::getDecimal()
{
	seed = (multiplier * Seed() + addition) % modulus;
	return seed / float(modulus);
}

std::string Cross::Random::getKey(const std::string& chars, const int length)
{
	std::string key = "";
	for (int i = 0; i < length; i++)
	{
		key += chars[Seed() % chars.length()];
	}
	return key;
}

float Cross::Random::getUniform(float a, float b)
{
	return getDecimal() * (b - a) + a;
}

float Cross::Random::getExponential(float beta)
{
	return exponentialDist(beta, getDecimal());
}

float Cross::Random::getWeibull(float a, float b, float c)
{
	return weibullDist(a, b, c, getDecimal());
}

float Cross::Random::getNormal(float mean, float variance)
{
	float x;
	float u;
	do
	{
		x = Random::exponentialDist(1.0, getDecimal());
		u = getDecimal();
	}
	while (u < Random::normal(0.0, .2f, x) / (2.0 * Random::exponentialDist(1.0, x)));
	return Random::normal(0.0, .2f, x);
}
