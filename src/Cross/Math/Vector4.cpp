#include <Cross/Math/Vector4.h>
#include <Cross/Math/Vector2.h>
#include <Cross/Math/Vector3.h>
#include <Cross/Math/Matrix.h>
#include <Cross/Math/Math.h>

const Cross::Vector4 Cross::Vector4::zero = Vector4(0, 0, 0, 0);
const Cross::Vector4 Cross::Vector4::one = Vector4(1, 1, 1, 1);

float Cross::Vector4::distance(const Vector4& vectorA, const Vector4& vectorB)
{
	return Math::sqrt((vectorA.x - vectorB.x) * (vectorA.x - vectorB.x) + (vectorA.y - vectorB.y) * (vectorA.y - vectorB.y) + (vectorA.z - vectorB.z) * (vectorA.z - vectorB.z)
		 + (vectorA.w - vectorB.w) * (vectorA.w - vectorB.w));
}

float Cross::Vector4::distanceSquared(const Vector4& vectorA, const Vector4& vectorB)
{
	return (vectorA.x - vectorB.x) * (vectorA.x - vectorB.x) + (vectorA.y - vectorB.y) * (vectorA.y - vectorB.y) + (vectorA.z - vectorB.z) * (vectorA.z - vectorB.z)
		+ (vectorA.w - vectorB.w) * (vectorA.w - vectorB.w);
}

float Cross::Vector4::distanceManhattan(const Vector4& vectorA, const Vector4& vectorB)
{
	return Math::abs(vectorA.x - vectorB.x) + Math::abs(vectorA.y - vectorB.y) + Math::abs(vectorA.z - vectorB.z) + Math::abs(vectorA.w - vectorA.w);
}

Cross::Vector4 Cross::Vector4::normalized(const Vector4& vector)
{
	Vector4 result = vector;
	result.normalize();
	return result;
}

float Cross::Vector4::dotProduct(const Cross::Vector4& vectorA, const Vector4& vectorB)
{
	return (vectorA.x * vectorB.x) + (vectorA.y * vectorB.y) + (vectorA.z * vectorB.z) + (vectorA.w * vectorB.w);
}

Cross::Vector4::Vector4()
{
	x = y = z = w = 0;
}

Cross::Vector4::Vector4(const Vector4& copy)
{
	x = copy.x;
	y = copy.y;
	z = copy.z;
	w = copy.w;
}

Cross::Vector4::Vector4(float x, float y, float z, float w)
{
	this->x = x;
	this->y = y;
	this->z = z;
	this->w = w;
}

Cross::Vector4::operator Cross::Vector2() const
{
	return Vector2(x, y);
}

Cross::Vector4::operator Cross::Vector3() const
{
	return Vector3(x, y, z);
}

Cross::Vector4& Cross::Vector4::operator=(const Vector4& operand)
{
	x = operand.x;
	y = operand.y;
	z = operand.z;
	w = operand.w;
	return *this;
}

float& Cross::Vector4::operator[](unsigned int index)
{
	return m[index];
}

const float Cross::Vector4::operator[](unsigned int index) const
{
	return m[index];
}

Cross::Vector4 Cross::Vector4::operator*(const float operand) const
{
	return Vector4(x * operand, y * operand, z * operand, w * operand);
}

Cross::Vector4& Cross::Vector4::operator*=(const float operand)
{
	this->x *= operand;
	this->y *= operand;
	this->z *= operand;
	this->w *= operand;
	return *this;
}

Cross::Vector4 Cross::Vector4::operator*(const Matrix& operand) const
{
	return Vector4(x * operand.m11 + y * operand.m21 + z * operand.m31 + w * operand.m41,
				   x * operand.m12 + y * operand.m22 + z * operand.m32 + w * operand.m42,
				   x * operand.m13 + y * operand.m23 + z * operand.m33 + w * operand.m43,
				   x * operand.m14 + y * operand.m24 + z * operand.m34 + w * operand.m44);
}

Cross::Vector4& Cross::Vector4::operator*=(const Matrix& operand)
{
	*this = *this * operand;
	return *this;
}

Cross::Vector4 Cross::Vector4::operator/(const float operand) const
{
	return Vector4(x / operand, y / operand, z / operand, w / operand);
}

Cross::Vector4& Cross::Vector4::operator/=(const float operand)
{
	this->x /= operand;
	this->y /= operand;
	this->z /= operand;
	this->w /= operand;
	return *this;
}

Cross::Vector4 Cross::Vector4::operator+(const Vector4& operand) const
{
	return Vector4(x + operand.x, y + operand.y, z + operand.z, w + operand.w);
}

Cross::Vector4& Cross::Vector4::operator+=(const Vector4& operand)
{
	this->x += operand.x;
	this->y += operand.y;
	this->z += operand.z;
	this->w += operand.w;
	return *this;
}

Cross::Vector4 Cross::Vector4::operator-() const
{
	return Vector4(-x, -y, -z, -w);
}

Cross::Vector4 Cross::Vector4::operator-(const Vector4& operand) const
{
	return Vector4(x - operand.x, y - operand.y, z - operand.z, w - operand.w);
}

Cross::Vector4& Cross::Vector4::operator-=(const Vector4& operand)
{
	this->x -= operand.x;
	this->y -= operand.y;
	this->z -= operand.z;
	this->w -= operand.w;
	return *this;
}

bool Cross::Vector4::operator==(const Vector4& operand) const
{
	return (x == operand.x && y == operand.y && z == operand.z && w == operand.w);
}

bool Cross::Vector4::operator!=(const Vector4& operand) const
{
	return (x != operand.x || y != operand.y || z != operand.z || w != operand.w);
}

void Cross::Vector4::set(const float x, const float y, const float z, const float w)
{
	this->x = x;
	this->y = y;
	this->z = z;
	this->w = w;
}

void Cross::Vector4::clear()
{
	x = y = z = w = 0;
}

float Cross::Vector4::getLength() const
{
	return Math::sqrt(x * x + y * y + z * z + w * w);
}

float Cross::Vector4::getLengthSquared() const
{
	return x * x + y * y + z * z + w * w;
}

float Cross::Vector4::getLengthManhattan() const
{
	return Math::abs(x) + Math::abs(y) + Math::abs(z) + Math::abs(w);
}

void Cross::Vector4::normalize()
{
	float inverseMagnitude = 1 / getLength();
	x *= inverseMagnitude;
	y *= inverseMagnitude;
	z *= inverseMagnitude;
	w *= inverseMagnitude;
}