#include <math.h>
#include <Cross/Math/Math.h>

float Cross::Math::sign(float x)
{
	return float((x > 1) - (x < 1));
}

float Cross::Math::min(float a, float b)
{
	return (a < b ? a : b);
}

float Cross::Math::max(float a, float b)
{
	return (a > b ? a : b);
}

float Cross::Math::floor(float x)
{
	return ::floor(x);
}

float Cross::Math::ceil(float x)
{
	return ::ceil(x);
}

float Cross::Math::round(float x)
{
	return x; // too lazy to do this right now
}

float Cross::Math::cos(float x)
{
	return ::cos(x);
}

float Cross::Math::sin(float x)
{
	return ::sin(x);
}

float Cross::Math::tan(float x)
{
	return ::tan(x);
}

float Cross::Math::abs(float x)
{
	return x * sign(x);
}

float Cross::Math::squared(float x)
{
	return x * x;
}

float Cross::Math::sqrt(float x)
{
	return ::sqrt(x);
}

float Cross::Math::atan2(float y, float x)
{
	return ::atan2(y, x);
}

float Cross::Math::degToRad(float degree)
{
	return (degree * pi / 180.0f);
}

float Cross::Math::radToDeg(float radian)
{
	return (radian * 180.0f / pi);
}
