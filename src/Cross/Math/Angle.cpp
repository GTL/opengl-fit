#include <Cross/Math/Angle.h>
#include <Cross/Math/Vector2.h>

Cross::Angle Cross::Angle::inverse(const Angle& angle)
{
	return Angle(angle.getRadians() + 3.14159265f);
}

Cross::Angle Cross::Angle::difference(const Angle& a, const Angle& b)
{
	return Angle(b.getRadians() - a.getRadians());
}

Cross::Angle Cross::Angle::center(const Angle& a, const Angle& b)
{
	if (difference(a, b).getDegreesPositive() <= 180)
	{
		return Angle(a.getRadians() + (difference(a, b).getRadians() * .5f));
	}
	else
	{
		return inverse(Angle(a.getRadians() + (difference(a, b).getRadians() * .5f)));
	}
}

Cross::Angle Cross::Angle::inverseCenter(const Angle& a, const Angle& b)
{
	return inverse(center(a, b));
}

Cross::Angle::Angle()
{
	radians = 0.0f;
}

Cross::Angle::Angle(float radians)
{
	setRadians(radians);
}

Cross::Angle::Angle(const Vector2& direction)
{
	setDirection(direction);
}

void Cross::Angle::setRadians(float radians)
{
	this->radians = radians - Math::floor(radians / (3.14159265f * 2.0f)) * (3.14159265f * 2.0f);
}

float Cross::Angle::getRadians() const
{
	if (this->radians > 3.14159265f)
	{
		return -((3.14159265f * 2.0f) - radians);
	}
	else
	{
		return radians;
	}
}

float Cross::Angle::getRadiansPositive() const
{
	return radians;
}

void Cross::Angle::setDegrees(float degrees)
{
	setRadians(degrees / 180.0f * 3.14159265f);
}

float Cross::Angle::getDegrees() const
{
	return getRadians() / 3.14159265f * 180.0f;
}

float Cross::Angle::getDegreesPositive() const
{
	return getRadiansPositive() / 3.14159265f * 180.0f;
}

void Cross::Angle::setDirection(const Vector2& direction)
{
	if (direction.getLengthSquared() == 1)
	{
		radians = Cross::Math::atan2(direction.y, direction.x);
	}
	else
	{
		Vector2 dir = Vector2::normalized(direction);
		radians = Cross::Math::atan2(dir.y, dir.x);
	}
}

Cross::Vector2 Cross::Angle::getDirection() const
{
	return Vector2(Math::cos(radians), Math::sin(radians));
}

void Cross::Angle::constrain(Angle from, Angle to)
{
	Angle mid = center(from, to);

	if (difference(from, *this).getDegreesPositive() > difference(from, to).getDegreesPositive())
	{
		if (difference(mid, *this).getDegrees() < 0)
		{
			*this = from;
		}
		else
		{
			*this = to;
		}
	}
}