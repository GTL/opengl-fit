#include <Cross/Math/Vector2.h>
#include <Cross/Math/Matrix.h>
#include <Cross/Math/Math.h>

const Cross::Vector2 Cross::Vector2::zero = Vector2(0, 0);
const Cross::Vector2 Cross::Vector2::one = Vector2(1, 1);

float Cross::Vector2::distance(const Vector2& vectorA, const Vector2& vectorB)
{
	return Math::sqrt((vectorA.x - vectorB.x) * (vectorA.x - vectorB.x) + (vectorA.y - vectorB.y) * (vectorA.y - vectorB.y));
}

float Cross::Vector2::distanceSquared(const Vector2& vectorA, const Vector2& vectorB)
{
	return (vectorA.x - vectorB.x) * (vectorA.x - vectorB.x) + (vectorA.y - vectorB.y) * (vectorA.y - vectorB.y);
}

float Cross::Vector2::distanceManhattan(const Vector2& vectorA, const Vector2& vectorB)
{
	return Math::abs(vectorA.x - vectorB.x) + Math::abs(vectorA.y - vectorB.y);
}

Cross::Vector2 Cross::Vector2::normalized(const Vector2& vector)
{
	Vector2 result = vector;
	result.normalize();
	return result;
}

float Cross::Vector2::dotProduct(const Vector2& vectorA, const Vector2& vectorB)
{
	return (vectorA.x * vectorB.x) + (vectorA.y * vectorB.y);
}

float Cross::Vector2::perpProduct(const Vector2& vectorA, const Vector2& vectorB)
{
	return vectorA.x * vectorB.y - vectorA.y * vectorB.x;
}

Cross::Vector2::Vector2()
{
	x = y = 0;
}

Cross::Vector2::Vector2(const Vector2& copy)
{
	x = copy.x;
	y = copy.y;
}

Cross::Vector2::Vector2(float x, float y)
{
	this->x = x;
	this->y = y;
}

Cross::Vector2& Cross::Vector2::operator=(const Vector2& operand)
{
	x = operand.x;
	y = operand.y;
	return *this;
}

float& Cross::Vector2::operator[](unsigned int index)
{
	return m[index];
}

const float Cross::Vector2::operator[](unsigned int index) const
{
	return m[index];
}

Cross::Vector2 Cross::Vector2::operator*(const float operand) const
{
	return Vector2(x * operand, y * operand);
}

Cross::Vector2& Cross::Vector2::operator*=(const float operand)
{
	this->x *= operand;
	this->y *= operand;
	return *this;
}

Cross::Vector2 Cross::Vector2::operator*(const Vector2& operand) const
{
	return Vector2(x * operand.x, y * operand.y);
}

Cross::Vector2& Cross::Vector2::operator*=(const Vector2& operand)
{
	x *= operand.x;
	y *= operand.y;
	return *this;
}

Cross::Vector2 Cross::Vector2::operator*(const Matrix& operand) const
{
	return Vector2(x * operand.m11 + y * operand.m21 + operand.m31 + operand.m41,
				   x * operand.m12 + y * operand.m22 + operand.m32 + operand.m42);
}

Cross::Vector2& Cross::Vector2::operator*=(const Matrix& operand)
{
	*this = *this * operand;
	return *this;
}

Cross::Vector2 Cross::Vector2::operator/(const float operand) const
{
	return Vector2(x / operand, y / operand);
}

Cross::Vector2& Cross::Vector2::operator/=(const float operand)
{
	this->x /= operand;
	this->y /= operand;
	return *this;
}

Cross::Vector2 Cross::Vector2::operator+(const Vector2& operand) const
{
	return Vector2(x + operand.x, y + operand.y);
}

Cross::Vector2& Cross::Vector2::operator+=(const Vector2& operand)
{
	this->x += operand.x;
	this->y += operand.y;
	return *this;
}

Cross::Vector2 Cross::Vector2::operator-() const
{
	return Vector2(-x, -y);
}

Cross::Vector2 Cross::Vector2::operator-(const Vector2& operand) const
{
	return Vector2(x - operand.x, y - operand.y);
}

Cross::Vector2& Cross::Vector2::operator-=(const Vector2& operand)
{
	this->x -= operand.x;
	this->y -= operand.y;
	return *this;
}

Cross::Vector2 Cross::Vector2::operator%(const Vector2& operand) const
{
	return Vector2(y - operand.y, operand.x - x);
}

Cross::Vector2& Cross::Vector2::operator%=(const Vector2& operand)
{
	*this = *this % operand;
	return *this;
}

bool Cross::Vector2::operator==(const Vector2& operand) const
{
	return (x == operand.x && y == operand.y);
}

bool Cross::Vector2::operator!=(const Vector2& operand) const
{
	return (x != operand.x || y != operand.y);
}

void Cross::Vector2::set(float x, float y)
{
	this->x = x;
	this->y = y;
}

void Cross::Vector2::clear()
{
	x = y = 0;
}

float Cross::Vector2::getLength() const
{
	return Math::sqrt(x * x + y * y);
}

float Cross::Vector2::getLengthSquared() const
{
	return x * x + y * y;
}

float Cross::Vector2::getLengthManhattan() const
{
	return Math::abs(x) + Math::abs(y);
}

void Cross::Vector2::normalize()
{
	float length = getLength();
	if (length == 0)
	{
		return;
	}
	float inverseMagnitude = 1 / length;
	x *= inverseMagnitude;
	y *= inverseMagnitude;
}

float Cross::Vector2::getDirection() const
{
	return Math::atan2(y, x);
}

void Cross::Vector2::setDirection(float angle)
{
	float magnitude = getLength();
	x = Math::cos(angle) * magnitude;
	y = Math::sin(angle) * magnitude;
}