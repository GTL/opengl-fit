#include <Cross/Application.h>
#include <Cross/Window.h>
#import "OpenGLView.h"
#import "CrossApp.h"

@implementation CrossEngineAppDelegate

@synthesize window;

- (void)applicationDidFinishLaunching:(NSNotification *)aNotification
{
	[window setTitle: @"Cross Framework"];
	[[window standardWindowButton:NSWindowMiniaturizeButton] setEnabled:YES];
	[[window standardWindowButton:NSWindowZoomButton] setEnabled:NO];
	
	NSRect resize;
	resize.origin.x = 0; resize.origin.y = 0;
	resize.size.width = 0; resize.size.height = 0;
	resize = [(id)window frameRectForContentRect:resize styleMask:NSTitledWindowMask];
	NSRect windowFrame = [window frame];
	windowFrame.size.width = 800 + resize.size.width;
	windowFrame.size.height = 600 + resize.size.height;
	[window setFrame:windowFrame display:YES];
	
	Cross::Application::getApplication()->window = new Cross::Window(window);
	Cross::Application::getApplication()->initialize();
}

- (BOOL)applicationShouldTerminateAfterLastWindowClosed:(NSApplication *)theApplication
{
	return YES;
}

@end
