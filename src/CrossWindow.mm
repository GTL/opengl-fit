#include <Cross/Window.h>
#import <CrossWindow.h>
#import <Foundation/Foundation.h>

@implementation CrossWindow
- (void)setCrossWindow:(void*)window
{
	win = window;
	[self setAcceptsMouseMovedEvents:YES];
	lastFlags = 0;
}

- (void)keyDown:(NSEvent*)anEvent
{
	Cross::Window::KeyEvent event;
	event.key = [anEvent keyCode];
	((Cross::Window*)win)->callKeyDown(event);
}

- (void)keyUp:(NSEvent*)anEvent
{
	Cross::Window::KeyEvent event;
	event.key = [anEvent keyCode];
	((Cross::Window*)win)->callKeyUp(event);
}

- (void)flagsChanged:(NSEvent *)anEvent
{
	unsigned int flags = [anEvent modifierFlags];
	unsigned int changed = flags ^ lastFlags;
	if (changed & NSShiftKeyMask)
	{
		if (flags & NSShiftKeyMask)
		{
			Cross::Window::KeyEvent event;
			event.key = [anEvent keyCode];
			((Cross::Window*)win)->callKeyDown(event);
		}
		else
		{
			Cross::Window::KeyEvent event;
			event.key = [anEvent keyCode];
			((Cross::Window*)win)->callKeyUp(event);
		}

	}
	lastFlags = [anEvent modifierFlags];
}

- (void)mouseMoved:(NSEvent *)anEvent
{
	Cross::Window::MouseMoveEvent event;
	event.x = [anEvent locationInWindow].x;
	event.y = ((Cross::Window*)win)->getHeight() - [anEvent locationInWindow].y;
	((Cross::Window*)win)->callMouseMove(event);
}

@end